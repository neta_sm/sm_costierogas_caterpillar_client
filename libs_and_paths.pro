QT += \
	core \
	gui \
	network \
	multimedia \
        widgets \
        concurrent

CONFIG += link_pkgconfig
UI_DIR = /tmp
Release:BIN_SUBDIR=release
Debug:BIN_SUBDIR=debug

INCLUDEPATH += /opt/brew/include
CONFIG += c++14

#PKGCONFIG += sdl2
DEFINES += DISABLE_JOY=1
