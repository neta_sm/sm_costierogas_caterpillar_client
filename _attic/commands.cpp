#include "commands.h"
#include <QtEndian>
#include <QtDebug>

#define CMDID_GET_VERSION 0x10
#define CMDID_INIT_DRIVER 0x01
#define CMDID_ENABLE_DRIVER 0x02
#define CMDID_SET_DRIVER_SPEED 0x03
#define CMDID_GET_RPM 0x04
#define CMDID_ENABLE_12V_RAIL 0x05
#define CMDID_SET_VIDEO_FEED 0x06
#define CMDID_SET_LEDS 0x07
#define CMDID_SHOOT_SEQUENCE 0x08
#define CMDID_LED_PULSE 0x09

CommandFactory::CommandFactory(): m_ctr(0) {}

template<typename T> void add(QByteArray &data, T n)
{
    T tmp = qToBigEndian(n);
    data.append((const char*)&tmp, sizeof(T));
}

void CommandFactory::get_version(QByteArray &data)
{
    data.clear();
    add<quint8>(data, CMDID_GET_VERSION);
    add<quint16>(data, 0);
    add<quint32>(data, get_seq());
}

void CommandFactory::init_driver(QByteArray &data, quint8 driver, quint16 aramp, quint16 dramp, quint16 max_rpm)
{
    data.clear();
    add<quint8>(data, CMDID_INIT_DRIVER);
    add<quint16>(data, 0);
    add<quint32>(data, get_seq());
    add<quint8>(data, driver);
    add<quint16>(data, aramp);
    add<quint16>(data, dramp);
    add<quint16>(data, max_rpm);
}

void CommandFactory::enable_driver(QByteArray &data, quint8 driver, bool active)
{
    data.clear();
    add<quint8>(data, CMDID_ENABLE_DRIVER);
    add<quint16>(data, 0);
    add<quint32>(data, get_seq());
    add<quint8>(data, driver);
    add<quint8>(data, active ? 1 : 0);
}

void CommandFactory::set_rpm(QByteArray &data, quint8 driver, qint16 rpm)
{
    data.clear();
    add<quint8>(data, CMDID_SET_DRIVER_SPEED);
    add<quint16>(data, 0);
    add<quint32>(data, get_seq());
    add<quint8>(data, driver);
    add<qint16>(data, rpm);
}

void CommandFactory::enable_12v_rail(QByteArray &data, bool active)
{
    data.clear();
    add<quint8>(data, CMDID_ENABLE_12V_RAIL);
    add<quint16>(data, 0);
    add<quint32>(data, get_seq());
    add<quint8>(data, active ? 1 : 0);
}

void CommandFactory::set_video_feed(QByteArray &data,
                                    bool active,
                                    quint8 camera,
                                    bool vflip, bool hflip,
                                    quint16 width, quint16 height,
                                    quint8 fps_num, quint8 fps_den,
                                    quint8 shutter_num, quint8 shutter_den,
                                    quint16 iso,
                                    quint8 settle_time_s,
                                    bool record
                                    )
{
    data.clear();
    add<quint8>(data, CMDID_SET_VIDEO_FEED);
    add<quint16>(data, 0);
    add<quint32>(data, get_seq());
    add<quint8>(data, active ? 1 : 0);
    add<quint8>(data, camera);
    unsigned int flip_xy = (vflip ? 0x01 : 0x00) | (hflip ? 0x02 : 0x00);
    add<quint8>(data, flip_xy);
    bool auto_mode = height == 0 && width == 0 && fps_den == 0 && shutter_den == 0 && iso == 0;
    add<quint8>(data, auto_mode ? 0x01 : 0x00);

    add<quint16>(data, width);
    add<quint16>(data, height);

    add<quint8>(data, fps_num);
    add<quint8>(data, fps_den);

    add<quint8>(data, shutter_num);
    add<quint8>(data, shutter_den);

    add<quint16>(data, iso);
    add<quint8>(data, settle_time_s);
    add<quint8>(data, record ? 1 : 0);
}

void CommandFactory::set_leds(QByteArray &data, unsigned int mask)
{
    data.clear();
    add<quint8>(data, CMDID_SET_LEDS);
    add<quint16>(data, 0);
    add<quint32>(data, get_seq());
    add<quint32>(data, mask);
}

void CommandFactory::pulse_leds(QByteArray &data, unsigned int mask, unsigned int pulses,
                                unsigned int pulse_len, unsigned int pulse_del)
{
    data.clear();
    add<quint8>(data, CMDID_LED_PULSE);
    add<quint16>(data, 0);
    add<quint32>(data, get_seq());
    add<quint16>(data, mask);
    add<quint8>(data, pulses);
    add<quint16>(data, pulse_len);
    add<quint16>(data, pulse_del);
}

void CommandFactory::shoot_still_sequence(QByteArray &data, quint8 mode,
                                          bool vflip, bool hflip,
                                          quint16 width, quint16 height,
                                          quint8 fps_num, quint8 fps_den,
                                          quint8 shutter_num, quint8 shutter_den,
                                          quint16 iso,
                                          quint8 settle_time_s,
                                          quint8 format,
                                          unsigned int pulses,
                                          unsigned int pulse_len,
                                          unsigned int pulse_del,
                                          quint8 camera0,
                                          quint16 camera0_leds,
                                          quint8 camera1,
                                          quint16 camera1_leds,
                                          quint8 camera2,
                                          quint16 camera2_leds,
                                          quint8 camera3,
                                          quint16 camera3_leds,
                                          quint8 camera4,
                                          quint16 camera4_leds,
                                          quint8 camera5,
                                          quint16 camera5_leds,
                                          quint8 camera6,
                                          quint16 camera6_leds,
                                          quint8 camera7,
                                          quint16 camera7_leds
                                          )
{
    data.clear();
    add<quint8>(data, CMDID_SHOOT_SEQUENCE);
    add<quint16>(data, 0);
    add<quint32>(data, get_seq());

    add<quint8>(data, mode);

    bool auto_mode = height == 0 && width == 0 && fps_den == 0 && shutter_den == 0 && iso == 0;
    add<quint8>(data, auto_mode ? 0x01 : 0x00);

    add<quint16>(data, width);
    add<quint16>(data, height);

    add<quint8>(data, fps_num);
    add<quint8>(data, fps_den);

    add<quint8>(data, shutter_num);
    add<quint8>(data, shutter_den);

    add<quint16>(data, iso);
    add<quint8>(data, settle_time_s);
    add<quint8>(data, format);

    unsigned int flip_xy = (vflip ? 0x01 : 0x00) | (hflip ? 0x02 : 0x00);

    qDebug()
            << "camera0" << camera0
            << "camera1" << camera1
            << "camera2" << camera2
            << "camera3" << camera3;
    add<quint8>(data, camera0);
    add<quint8>(data, flip_xy);
    add<quint16>(data, camera0_leds);
    add<quint8>(data, camera1);
    add<quint8>(data, flip_xy);
    add<quint16>(data, camera1_leds);
    add<quint8>(data, camera2);
    add<quint8>(data, flip_xy);
    add<quint16>(data, camera2_leds);
    add<quint8>(data, camera3);
    add<quint8>(data, flip_xy);
    add<quint16>(data, camera3_leds);
    add<quint8>(data, camera4);
    add<quint8>(data, flip_xy);
    add<quint16>(data, camera4_leds);
    add<quint8>(data, camera5);
    add<quint8>(data, flip_xy);
    add<quint16>(data, camera5_leds);
    add<quint8>(data, camera6);
    add<quint8>(data, flip_xy);
    add<quint16>(data, camera6_leds);
    add<quint8>(data, camera7);
    add<quint8>(data, flip_xy);
    add<quint16>(data, camera7_leds);

    add<quint8>(data, pulses);
    add<quint16>(data, pulse_len);
    add<quint16>(data, pulse_del);
}


quint32 CommandFactory::get_seq()
{
    auto res = m_ctr;
    m_ctr ++;
    return res;
}
