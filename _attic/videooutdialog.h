#ifndef VIDEOOUTDIALOG_H
#define VIDEOOUTDIALOG_H

#include <QDialog>

namespace Ui {
class VideoOutDialog;
}

class VideoOutDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VideoOutDialog(QWidget *parent = 0);
    ~VideoOutDialog();

private:
    Ui::VideoOutDialog *ui;
};

#endif // VIDEOOUTDIALOG_H
