#ifndef VIDEO_H
#define VIDEO_H

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}

#include <string>
#include <memory>

struct VideoSupport {
    VideoSupport(const std::string &source);
    VideoSupport() = delete;
    VideoSupport(const VideoSupport &) = delete;
    ~VideoSupport();

private:
    std::string m_source;
    AVFormatContext *m_ctx_input;

    void open_input();
    void find_video_stream();
};

struct VideoError: std::runtime_error {
    using std::runtime_error::runtime_error;
};

#endif // VIDEO_H
