#include "videooutdialog.h"
#include "ui_videooutdialog.h"
#include <QtMultimediaWidgets/QVideoWidget>
#include <QtMultimedia/QMediaPlayer>
#include <QtMultimedia/QMediaPlaylist>
#include <QtMultimediaWidgets/QGraphicsVideoItem>

VideoOutDialog::VideoOutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VideoOutDialog)
{
    ui->setupUi(this);
    QMediaPlayer *player=new QMediaPlayer;
    player->setMedia(QUrl::fromLocalFile("/tmp/test.h264"));
    QGraphicsVideoItem *item = new QGraphicsVideoItem;
    player->setVideoOutput(item);
    ui->graphicsView->scene()->addItem(item);
    player->play();
}

VideoOutDialog::~VideoOutDialog()
{
    delete ui;
}
