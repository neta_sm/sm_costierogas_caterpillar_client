#include "caterpillardriver.h"
#include <QTime>
#include <QCoreApplication>

#define THROTTLE_TIME_MS 300

void delay( int millisecondsToWait )
{
    QTime dieTime = QTime::currentTime().addMSecs( millisecondsToWait );
    while( QTime::currentTime() < dieTime )
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
    }
}

CaterpillarDriver::CaterpillarDriver(QObject *parent, Options *o) : QObject(parent),
    m_options(o),
    m_socket(new QUdpSocket),
    m_control_last(nullptr),
    m_factory(new CommandFactory),
    m_delay_timer(new QTimer)
{
    m_socket->setSocketOption(QAbstractSocket::LowDelayOption, true);
    m_delay_timer->setSingleShot(true);
    connect(m_delay_timer, &QTimer::timeout, this, &CaterpillarDriver::sendDelayedCommand);
}

void CaterpillarDriver::sendDelayedCommand()
{
    if (cache_m1.size() > 0)
        sendCommand(cache_m1);
    if (cache_m2.size() > 0)
        sendCommand(cache_m2);
    if (cache_m3.size() > 0)
        sendCommand(cache_m3);
    if (cache_m4.size() > 0)
        sendCommand(cache_m4);
}

void CaterpillarDriver::rearm_timer()
{
    if (! m_delay_timer->isActive())
    {
        m_delay_timer->setInterval(THROTTLE_TIME_MS);
        m_delay_timer->start();
    }
}

CaterpillarDriver::~CaterpillarDriver()
{
    delete m_delay_timer;
    delete m_socket;
    if (m_control_last)
    {
        delete m_control_last;
    }
    delete m_factory;
}

void CaterpillarDriver::sendCommand(QByteArray cmd)
{
    QHostAddress host(m_options->host);
    qDebug() << "sending " << cmd.size() << "bytes command";
    m_socket->writeDatagram(cmd, host, m_options->port);
    m_socket->flush();
    cmd.clear();
}

void CaterpillarDriver::updateControl(const ControlStatus *control, bool fast)
{
    bool force = false;
    QByteArray cmd;
    if (!m_control_last)
    {
        m_control_last = new ControlStatus;
        force = true;
    }

    if (control->en12v != m_control_last->en12v || force)
    {
        m_factory->enable_12v_rail(cmd, control->en12v);
        sendCommand(cmd);
        m_control_last->en12v = control->en12v;
    }
    if (!m_control_last->en12v)
    {
        qInfo() << "12V rail disabled. can't do anything";
        return;
    }

    if (control->m1en != m_control_last->m1en || force)
    {
        if (control->m1en)
        {
            qInfo() << "initializing driver 1";
            m_factory->init_driver(cmd, 0, m_options->accel1, m_options->decel1, m_options->maxrpm1);
            sendCommand(cmd);
            m_factory->set_rpm(cmd, 0, 0);
            sendCommand(cmd);
        }

        qInfo() << (control->m1en ? "enabling" : "disabling") << " driver 1";
        m_factory->enable_driver(cmd, 0, control->m1en);
        sendCommand(cmd);

        m_control_last->m1en = control->m1en;
    }
    if (control->m2en != m_control_last->m2en || force)
    {
        if (control->m2en)
        {
            qInfo() << "initializing driver 2";
            m_factory->init_driver(cmd, 1, m_options->accel2, m_options->decel2, m_options->maxrpm2);
            sendCommand(cmd);
            m_factory->set_rpm(cmd, 1, 0);
            sendCommand(cmd);
        }

        qInfo() << (control->m2en ? "enabling" : "disabling") << " driver 2";
        m_factory->enable_driver(cmd, 1, control->m2en);
        sendCommand(cmd);

        m_control_last->m2en = control->m2en;
    }
    if (control->m3en != m_control_last->m3en || force)
    {
        if (control->m3en)
        {
            qInfo() << "initializing driver 3";
            m_factory->init_driver(cmd, 2, m_options->accel3, m_options->decel3, m_options->maxrpm3);
            sendCommand(cmd);
            m_factory->set_rpm(cmd, 2, 0);
            sendCommand(cmd);
        }

        qInfo() << (control->m3en ? "enabling" : "disabling") << " driver 3";
        m_factory->enable_driver(cmd, 2, control->m3en);
        sendCommand(cmd);

        m_control_last->m3en = control->m3en;
    }
    if (control->m4en != m_control_last->m4en || force)
    {
        if (control->m4en)
        {
            qInfo() << "initializing driver 4";
            m_factory->init_driver(cmd, 3, m_options->accel4, m_options->decel4, m_options->maxrpm4);
            sendCommand(cmd);
            m_factory->set_rpm(cmd, 3, 0);
            sendCommand(cmd);
        }

        qInfo() << (control->m4en ? "enabling" : "disabling") << " driver 4";
        m_factory->enable_driver(cmd, 3, control->m4en);
        sendCommand(cmd);

        m_control_last->m4en = control->m4en;
    }

    m_control_last->m1hold = control->m1hold;
    m_control_last->m2hold = control->m2hold;
    m_control_last->m3hold = control->m3hold;
    m_control_last->m4hold = control->m4hold;

    bool rpm_changed = false;
    if (!m_control_last->m1hold && m_control_last->m1en && (m_control_last->m1rpm != control->m1rpm || force))
    {
        qInfo() << "setting driver 1 speed:" << control->m1rpm << "rpm";
        m_factory->set_rpm(cmd, 0, control->m1rpm);
        if (!fast)
        {
            cache_m1 = cmd;
            rearm_timer();
        } else {
            sendCommand(cmd);
        }
        m_control_last->m1rpm = control->m1rpm;
        rpm_changed = true;
    }
    if (!m_control_last->m2hold && m_control_last->m2en && (m_control_last->m2rpm != control->m2rpm || force))
    {
        qInfo() << "setting driver 2 speed:" << control->m2rpm << "rpm";
        m_factory->set_rpm(cmd, 1, control->m2rpm);
        if (!fast)
        {
            cache_m2 = cmd;
            rearm_timer();
        } else {
            sendCommand(cmd);
        }
        m_control_last->m2rpm = control->m2rpm;
        rpm_changed = true;
    }
    if (!m_control_last->m3hold && m_control_last->m3en && (m_control_last->m3rpm != control->m3rpm || force))
    {
        qInfo() << "setting driver 3 speed:" << control->m3rpm << "rpm";
        m_factory->set_rpm(cmd, 2, control->m3rpm);
        if (!fast)
        {
            cache_m3 = cmd;
            rearm_timer();
        } else {
            sendCommand(cmd);
        }
        m_control_last->m3rpm = control->m3rpm;
        rpm_changed = true;
    }
    if (!m_control_last->m4hold && m_control_last->m4en && (m_control_last->m4rpm != control->m4rpm || force))
    {
        qInfo() << "setting driver 4 speed:" << control->m4rpm << "rpm";
        m_factory->set_rpm(cmd, 3, control->m4rpm);
        if (!fast)
        {
            cache_m4 = cmd;
            rearm_timer();
        } else {
            sendCommand(cmd);
        }
        m_control_last->m4rpm = control->m4rpm;
        rpm_changed = true;
    }


    if (rpm_changed)
    {
        emit statusChanged(m_control_last);
    }
}

void CaterpillarDriver::set_leds(unsigned int mask)
{
    QByteArray cmd;
    m_factory->set_leds(cmd, mask);
    sendCommand(cmd);
}


void CaterpillarDriver::pulse_leds(unsigned int mask, unsigned int pulses,
                unsigned int pulse_len, unsigned int pulse_del)
{
    QByteArray cmd;
    qDebug()
            << "mask:" << mask
            << "pulses:" << pulses
            << "len:" << pulse_len
            << "del:" << pulse_del;
    m_factory->pulse_leds(cmd, mask, pulses, pulse_len, pulse_del);
    sendCommand(cmd);
}

void CaterpillarDriver::set_video_feed(bool active,
                    quint8 camera,
                    bool vflip, bool hflip, quint16 width,
                    quint16 height,
                    quint8 fps_num, quint8 fps_den,
                    quint8 shutter_num, quint8 shutter_den,
                    quint16 iso,
                    quint8 settle_time_s,
                    bool record
                    )
{
    QByteArray cmd;
    m_factory->set_video_feed(cmd, active, camera,
                              vflip, hflip, width,
                              height,
                              fps_num, fps_den,
                              shutter_num, shutter_den, iso, settle_time_s, record);
    sendCommand(cmd);
}


void CaterpillarDriver::shoot_still_sequence(
                          quint8 mode,
                          bool vflip, bool hflip,
                          quint16 width, quint16 height,
                          quint8 fps_num, quint8 fps_den,
                          quint8 shutter_num, quint8 shutter_den,
                          quint16 iso,
                          quint8 settle_time_s,
                          quint8 format,
                          unsigned int pulses,
                          unsigned int pulse_len,
                          unsigned int pulse_del,
                          quint8 camera0,
                          quint16 camera0_leds,
                          quint8 camera1,
                          quint16 camera1_leds,
                          quint8 camera2,
                          quint16 camera2_leds,
                          quint8 camera3,
                          quint16 camera3_leds,
                          quint8 camera4,
                          quint16 camera4_leds,
                          quint8 camera5,
                          quint16 camera5_leds,
                          quint8 camera6,
                          quint16 camera6_leds,
                          quint8 camera7,
                          quint16 camera7_leds
                          )
{
    QByteArray cmd;
    m_factory->shoot_still_sequence(cmd,
        mode,
        vflip, hflip,
        width, height,
        fps_num, fps_den,
        shutter_num, shutter_den,
        iso,
        settle_time_s,
        format,
        pulses,
        pulse_len,
        pulse_del,
        camera0,
        camera0_leds,
        camera1,
        camera1_leds,
        camera2,
        camera2_leds,
        camera3,
        camera3_leds,
        camera4,
        camera4_leds,
        camera5,
        camera5_leds,
        camera6,
        camera6_leds,
        camera7,
        camera7_leds
    );

    sendCommand(cmd);
}
