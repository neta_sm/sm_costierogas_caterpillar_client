#ifndef JOYMAINWINDOW_H
#define JOYMAINWINDOW_H

#include <QMainWindow>
#include "sdljoy.h"

namespace Ui {
class JoyMainWindow;
}

class JoyMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit JoyMainWindow(QWidget *parent = 0);
    ~JoyMainWindow();

private:
    JoystickInput *m_joy;
    Ui::JoyMainWindow *ui;
};

#endif // JOYMAINWINDOW_H
