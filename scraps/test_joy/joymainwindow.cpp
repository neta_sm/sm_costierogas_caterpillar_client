#include "joymainwindow.h"
#include "ui_joymainwindow.h"
#include <chrono>
#include "QDebug"

JoyMainWindow::JoyMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::JoyMainWindow)
{
    ui->setupUi(this);
    m_joy = new JoystickInput(this);
    m_joy->selectDevice();
    connect(m_joy, &JoystickInput::axisHasChanged, [this](auto){
        auto status = m_joy->last();
        ui->ax_1->setText(QString().sprintf("%d", status->axis_1));
        ui->ax_2->setText(QString().sprintf("%d", status->axis_2));
        ui->ax_3->setText(QString().sprintf("%d", status->axis_3));
    });
}

JoyMainWindow::~JoyMainWindow()
{
    delete ui;
}
