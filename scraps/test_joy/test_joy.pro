TARGET = test_joy
SOURCES = test_joy.cpp \
    joymainwindow.cpp
include(../../libs_and_paths.pro)
INCLUDEPATH += ../../json/src
INCLUDEPATH += ../../common
INCLUDEPATH += ../../spdlog/include
INCLUDEPATH += $$UI_DIR
LIBS += -L../../common -L../../common/$$BIN_SUBDIR -lcommon

FORMS += \
    joymainwindow.ui

HEADERS += \
    joymainwindow.h

