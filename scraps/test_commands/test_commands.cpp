#include "json.hpp"
#include "logging.hpp"
#include <iostream>
#include "options.h"
#include <QApplication>


module_logger("test_commands");

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Options options;
    options.load();

    OptionsDialog optionsDialog;
    int ok = optionsDialog.display(&options);
    if (ok)
    {
        options.persist();

        //JoyThread joy_thread;
        //joy_thread.start();


        return a.exec();
    }
    return ok != 0;
}
