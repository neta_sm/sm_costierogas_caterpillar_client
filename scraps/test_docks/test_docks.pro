TARGET = test_docks
SOURCES = test_docks.cpp \
    dockmainwindow.cpp
include(../../libs_and_paths.pro)
INCLUDEPATH += ../../json/src
INCLUDEPATH += ../../common
INCLUDEPATH += ../../spdlog/include
INCLUDEPATH += $$UI_DIR
LIBS += -L../../common -L../../common/$$BIN_SUBDIR -lcommon

FORMS += \
    dockmainwindow.ui

HEADERS += \
    dockmainwindow.h

