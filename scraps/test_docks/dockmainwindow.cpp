#include "dockmainwindow.h"
#include "ui_dockmainwindow.h"

DockMainWindow::DockMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DockMainWindow)
{
    ui->setupUi(this);
    tabifyDockWidget(ui->dockWidget_1, ui->dockWidget_2);
    tabifyDockWidget(ui->dockWidget_1, ui->dockWidget_3);
}

DockMainWindow::~DockMainWindow()
{
    delete ui;
}
