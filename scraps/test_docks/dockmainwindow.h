#ifndef DOCKMAINWINDOW_H
#define DOCKMAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class DockMainWindow;
}

class DockMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DockMainWindow(QWidget *parent = 0);
    ~DockMainWindow();

private:
    Ui::DockMainWindow *ui;
};

#endif // DOCKMAINWINDOW_H
