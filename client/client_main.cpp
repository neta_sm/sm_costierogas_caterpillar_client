#include "json.hpp"
#include "logging.hpp"
#include <iostream>
#include "options.h"
#include <QApplication>
#include <QThread>
#include <future>
#include "commands.hpp"
#include <random>
#include "stream_client.h"
#include "mainwindow.h"
#include "systemstatus.h"

module_logger("client");


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setApplicationName("caterpillar2");
    QApplication::setOrganizationName("scienzia.machinale");
    Options options;
    options.load();


    OptionsDialog optionsDialog;
    int ok = optionsDialog.display(&options);
    if (ok)
    {
        options.persist();

        SystemStatus sstatus;
        MainWindow mw(&options, &sstatus);
        mw.show();

        a.exec();
    }
    return ok != 0;
}
