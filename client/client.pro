TARGET = client 
SOURCES = \
    client_main.cpp
include(../libs_and_paths.pro)
INCLUDEPATH += ../json/src
INCLUDEPATH += ../common
INCLUDEPATH += ../shared
INCLUDEPATH += ../spdlog/include
INCLUDEPATH += ../msgpack-c/include
INCLUDEPATH += $$UI_DIR
LIBS += -L../common -L../common/$$BIN_SUBDIR -lcommon /opt/brew/lib/libboost_system-mt.a
CONFIG += console
