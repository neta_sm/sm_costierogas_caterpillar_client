#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_options.h"
#include "axisitem.h"
#include "commands.hpp"
#include "stream_client.h"
#include "systemstatus.h"
#include <iostream>
#include <fstream>

#include <QDebug>
#include <QResizeEvent>
#include <QDesktopServices>
#include <QSpacerItem>
#include <QSettings>


#define AXIS_MIN (-32768)
#define AXIS_MAX 32767
#define AXIS_NOISE 1000
#define Y_THR_BOOST (AXIS_MAX*2/3)

#define MOTOR_ID_TURN_ORIENT  1
#define MOTOR_ID_FOCUS_ORIENT 3
#define MOTOR_ID_FOCUS_FRONT  2
#define MOTOR_ID_FOCUS_BACK   4
//  #define UI_DEBUG 1
module_logger("main");

//#define AXIS_DEBUG 1

//static void create_led_controls()
//{
//    /*
//     *         led_1 = new QCheckBox(tabVisuals);
//        led_1->setObjectName(QStringLiteral("led_1"));
//        sizePolicy2.setHeightForWidth(led_1->sizePolicy().hasHeightForWidth());
//        led_1->setSizePolicy(sizePolicy2);

//        horizontalLayout_5->addWidget(led_1);
//        */
//}



const QEvent::Type EVENT_UI_INITIALIZE = static_cast<QEvent::Type>(QEvent::User + 1);

// Define your custom event subclass
class UIINitializeEvent : public QEvent
{
public:
    UIINitializeEvent(bool _initialize_ui):
        QEvent(EVENT_UI_INITIALIZE),
        initialize_ui(_initialize_ui)
    {}

    bool initialize_ui = false;
};


static double get_slider_position(QSlider *slider, double vmin, double vmax)
{
    double smin = slider->minimum();
    double smax = slider->maximum();
    double sgap = smax - smin;
    double spos = slider->value() - smin;
    double pct = spos / sgap;
    double vgap = vmax - vmin;
    return vmin + vgap * pct;
}

static void label_set_value(QLabel *l, double val)
{
    static char buf[1024];
    std::snprintf(buf, sizeof(buf), "%02.02f", val);
    l->setText(QString(buf));
}


MainWindow::MainWindow(Options *o, SystemStatus *ss, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_options(o),
    m_systemstatus(ss),
    m_streamclient(new stream_client(o, this))
{
    ui->setupUi(this);
    ui->centralWidget->hide(); // this allows the docks to go fill their parent. somehow
    tabifyDockWidget(ui->dock_status, ui->dock_navi);
    tabifyDockWidget(ui->dock_status, ui->dock_image);
    tabifyDockWidget(ui->dock_status, ui->dock_leds);
    tabifyDockWidget(ui->dock_status, ui->dock_vback);
    tabifyDockWidget(ui->dock_status, ui->dock_vfront);
    tabifyDockWidget(ui->dock_status, ui->dock_vorient);
    connect(ui->comboBox_camera, &QComboBox::currentTextChanged, [this](auto){
        this->readUpdatedStatus(m_systemstatus, false);
    });

    QSettings settings;
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("windowState").toByteArray());

    // focus 1st dock
    ui->dock_status->show();
    ui->dock_status->raise();
    // freeze everything. we'll unfreeze after 1st status acquire
#if !UI_DEBUG
    setEnabled(false);
#endif

    setTabPosition(Qt::AllDockWidgetAreas, QTabWidget::North);
    setDockOptions(dockOptions() | ForceTabbedDocks);

    // init. navigation widget
    {
        auto w = ui->graphicsView;
        auto scene = new QGraphicsScene(0, 0, 200, 200, w);
        scene->setBackgroundBrush(Qt::black);
        axes = new AxisItem(m_options);
        axes->setAxisRangeX  (AXIS_MIN, AXIS_MAX, 0);
        axes->setAxisRangeY  (AXIS_MIN, AXIS_MAX, 0);
        axes->setAxisRangeYaw(AXIS_MIN, AXIS_MAX, 0);
        scene->addItem(axes);
        w->setScene(scene);
    }


#if !DISABLE_JOY
    // init. joystick device
    m_joy = new JoystickInput(this);
    m_joy->selectDevice();
    connect(m_joy, &JoystickInput::axisHasChanged, [this](auto){
        this->joyChanged();
    });
#endif

    // init. UI timer
    m_controltimer = new QTimer(this);
    m_controltimer->setInterval(50);
    m_controltimer->setSingleShot(false);

    // init API ping timer
    m_pingtimer = new QTimer(this);
    m_pingtimer->setInterval(3000);
    m_pingtimer->setSingleShot(false);

    //m_caterpillar = new CaterpillarDriver(this, m_options);

    connect(m_controltimer, &QTimer::timeout, this, [this](){
        this->rescaleScene();
    });
    m_controltimer->start();

//    connect(m_caterpillar, &CaterpillarDriver::statusChanged, this, &MainWindow::readUpdatedStatus);
//    connect(ui->hold_axial, &QCheckBox::stateChanged, this, &MainWindow::enableChanged);
//    connect(ui->hold_left, &QCheckBox::stateChanged, this, &MainWindow::enableChanged);
//    connect(ui->hold_right, &QCheckBox::stateChanged, this, &MainWindow::enableChanged);
    connect(ui->btn_zero_jog, &QPushButton::clicked, this, &MainWindow::zeroJog);


    ui->hold_axial->setChecked(false);
    ui->hold_left->setChecked(false);
    ui->hold_right->setChecked(false);

#if AXIS_DEBUG
    connect(ui->dbgset, &QPushButton::pressed, this, &MainWindow::debugSimulatedEvent);
#else
    ui->dbgx->setHidden(true);
    ui->dbgy->setHidden(true);
    ui->dbgz->setHidden(true);
    ui->dbgset->setHidden(true);
#endif

    auto update_connection_status = [=]()
    {
        auto status = m_streamclient->get_status();
        const char *s = "not connected";
        switch (status)
        {
        case stream_client::status::connected:
            s = m_streamclient->peer_is_up_and_responsive() ? "connected" : "connected but unreliable";
            reread_status_asap = true;
            break;
        case stream_client::status::connecting:
#if !UI_DEBUG
            setEnabled(false);
#endif
            s = "connecting";
            break;
        case stream_client::status::error:
#if !UI_DEBUG
            setEnabled(false);
#endif
            s = "connection error";
            break;
        case stream_client::status::disconnected:
#if !UI_DEBUG
            setEnabled(false);
#endif
            s = "connection error";
            break;
        case stream_client::status::offline:
#if !UI_DEBUG
            setEnabled(false);
#endif
            s = "connection error";
            break;
        }
        ui->statusBar->showMessage(s);
        auto do_ping = status == stream_client::status::connected;
        if (m_pingtimer->isActive() != do_ping)
        {
            if (do_ping)
            {
                m_pingtimer->start();
            } else {
                m_pingtimer->stop();
            }
        }

    };
    connect(m_streamclient, &stream_client::connected, this, update_connection_status);
    connect(m_streamclient, &stream_client::disconnected, this, update_connection_status);
    connect(m_streamclient, &stream_client::responsive, this, update_connection_status);
    connect(m_streamclient, &stream_client::unresponsive, this, update_connection_status);
    update_connection_status();
    connect(m_streamclient, &stream_client::new_video_frame, this, &MainWindow::handle_new_video_frame);


    connect(m_pingtimer, &QTimer::timeout, this, [=](){
        m_pingtimer->stop();
        try {
            auto status = m_streamclient->get_status();
            if (status != stream_client::status::connected)
            {
                return;
            }
            if (reread_status_asap)
            {
                json payload = json::object();
                request_callback_t cb;
                if (!ui_initialized)
                {
                    cb = [this](const pending_request &r){
                        if (r.current_status == pending_request::status::completed_ok)
                        {
                            m_systemstatus->readAPIStatusStructure(r.reply_data);
                            ui_initialized = true;
                            reread_status_asap = false;
                            QApplication::postEvent(this, new UIINitializeEvent(true));
                        }
                    };
                    payload = json{
                        {"extended", true}
                    };
                } else {
                    cb = [this](const pending_request &r){
                        if (r.current_status == pending_request::status::completed_ok)
                        {
                            m_systemstatus->readAPIStatusStructure(r.reply_data);
                            m_systemstatus->dirty = false;
                            reread_status_asap = false;
                            QApplication::postEvent(this, new UIINitializeEvent(false));
                        }
                    };
                }
                m_streamclient->send_cmd<cmd_get_status>(payload, cb);
            } else {
                reread_status_asap = true;
                m_streamclient->cmd_ping();
            }
        } catch (...) {
        }
        m_pingtimer->start();
    });
    m_pingtimer->start();


    connect(ui->checkBox_power12v, &QCheckBox::stateChanged, [this](auto){
        // send 12v power rail change command
        auto status = ui->checkBox_power12v->checkState() == Qt::Checked;
        reread_status_asap = true;
        m_streamclient->async_send_cmd<cmd_power>({
            {"12v", status}
        });
    });

    connect(ui->pushButton_visuals_send, &QPushButton::clicked, this, [=](){
        this->send_video_params();
    });


    // init. video displays
    for (auto w: {ui->graphicsView_vfront, ui->graphicsView_vback, ui->graphicsView_vorient})
    {
        auto scene = new QGraphicsScene(0, 0, 1600, 1200, w);
        scene->setBackgroundBrush(Qt::darkGray);
        w->setScene(scene);
    }
    {
        auto vh = std::make_shared<VideoHarness>();
        vh->slot = "back";
        vh->enable = ui->checkBox_vback_enable;
        vh->save = ui->checkBox_vback_save;
        vh->view = ui->graphicsView_vback;
        auto focus_set = ui->pushButton_vback_focusset;
        auto focus_txt = ui->lineEdit_focus_back;
        auto focus_zero = ui->pushButton_vback_focuszero;
        auto MOTOR = MOTOR_ID_FOCUS_BACK;
        connect(vh->enable, &QCheckBox::stateChanged, [=](auto){
            this->setVideoFeedsStatus();
        });
        connect(focus_set, &QPushButton::clicked, [=](auto){
            this->sendLLMotorCommand(MOTOR, focus_txt);
        });
        connect(focus_zero, &QPushButton::clicked, [=](auto){
            this->setLLMotorZero(MOTOR);
        });
        vh->setEnabled(false);
        video_harnesses["back"] = vh;
    }
    {
        auto vh = std::make_shared<VideoHarness>();
        vh->slot = "front";
        vh->enable = ui->checkBox_vfront_enable;
        vh->save = ui->checkBox_vfront_save;
        vh->view = ui->graphicsView_vfront;
        auto focus_set = ui->pushButton_vfront_focusset;
        auto focus_txt = ui->lineEdit_focus_front;
        auto focus_zero = ui->pushButton_vfront_focuszero;
        auto MOTOR = MOTOR_ID_FOCUS_FRONT;
        connect(vh->enable, &QCheckBox::stateChanged, [=](auto){
            this->setVideoFeedsStatus();
        });
        connect(focus_set, &QPushButton::clicked, [=](auto){
            this->sendLLMotorCommand(MOTOR, focus_txt);
        });
        connect(focus_zero, &QPushButton::clicked, [=](auto){
            this->setLLMotorZero(MOTOR);
        });
        vh->setEnabled(false);
        video_harnesses["front"] = vh;
    }
    {
        auto vh = std::make_shared<VideoHarness>();
        vh->slot = "orient";
        vh->enable = ui->checkBox_vorient_enable;
        vh->save = ui->checkBox_vorient_save;
        vh->view = ui->graphicsView_vorient;
        auto focus_set = ui->pushButton_vorient_focusset;
        auto focus_txt = ui->lineEdit_focus_orient;
        auto focus_zero = ui->pushButton_vorient_focuszero;
        auto MOTOR = MOTOR_ID_FOCUS_ORIENT;
        connect(vh->enable, &QCheckBox::stateChanged, [=](auto){
            this->setVideoFeedsStatus();
        });
        connect(focus_set, &QPushButton::clicked, [=](auto){
            this->sendLLMotorCommand(MOTOR, focus_txt);
        });
        connect(focus_zero, &QPushButton::clicked, [=](auto){
            this->setLLMotorZero(MOTOR);
        });
        vh->setEnabled(false);
        video_harnesses["orient"] = vh;

        auto turn_set = ui->pushButton_vorient_turnset;
        auto turn_txt = ui->lineEdit_turn_orient;
        auto turn_zero = ui->pushButton_vorient_turnzero;
        connect(turn_set, &QPushButton::clicked, [=](auto){
            this->sendLLMotorCommand(MOTOR_ID_TURN_ORIENT, turn_txt);
        });
        connect(turn_zero, &QPushButton::clicked, [=](auto){
            this->setLLMotorZero(MOTOR_ID_TURN_ORIENT);
        });
    }

    connect(ui->horizontalSlider_brightness,  &QSlider::valueChanged, this, &MainWindow::update_slider_labels);
    connect(ui->horizontalSlider_exposure_us, &QSlider::valueChanged, this, &MainWindow::update_slider_labels);
    connect(ui->horizontalSlider_gamma,       &QSlider::valueChanged, this, &MainWindow::update_slider_labels);
    connect(ui->horizontalSlider_gain,        &QSlider::valueChanged, this, &MainWindow::update_slider_labels);
    connect(ui->horizontalSlider_framerate,   &QSlider::valueChanged, this, &MainWindow::update_slider_labels);
    connect(ui->horizontalSlider_contrast,    &QSlider::valueChanged, this, &MainWindow::update_slider_labels);
    connect(ui->checkBox_battery_charger,     &QCheckBox::stateChanged, [=](auto){
        if (ui->checkBox_battery_charger->checkState() == Qt::Unchecked)
        {
            this->m_streamclient->async_send_cmd<cmd_power>({
                                                {"battery_charger", false},
                                            });
        }
    });


    m_streamclient->start_connection();
}


bool MainWindow::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::FocusIn)
    {
//        QString slot_name;
//        if (object == ui->dock_vback)
//        {
//            slot_name = "back";
//        }
//        if (object == ui->dock_vfront)
//        {
//            slot_name = "front";
//        }
//        if (object == ui->dock_vorient)
//        {
//            slot_name = "orient";
//        }
//        if (slot_name.size() > 0)
//        {
//            QComboBox *c = ui->comboBox_camera;
//            c->setCurrentText(slot_name);
//        }
    }
    return false;
}

void MainWindow::rescaleScene()
{
    auto w = ui->graphicsView;
    w->setSceneRect(0, 0, w->frameSize().width(), w->frameSize().height());
    w->fitInView(w->scene()->itemsBoundingRect());
}


void MainWindow::resizeEvent(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    rescaleScene();
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::sendLLMotorCommand(int motor, QLineEdit *txt)
{
    std::string m_n = std::to_string(motor);
    bool ok;
    int pos = txt->text().toInt(&ok);
    if (!ok)
    {
        lwarning("bad motor parameters. message ignored");
        return;
    }
    linfo("motor move request for motor {}, pos={}", motor, pos);
    m_streamclient->async_send_cmd<cmd_motor_goto>({
                                                {m_n, pos}
                                            });
}

void MainWindow::setLLMotorZero(int motor)
{
    std::string m_n = std::to_string(motor);
    int pos = 0;
    linfo("motor move request for motor {}, pos={}", motor, pos);
    m_streamclient->async_send_cmd<cmd_motor_set_zero>({
                                                {m_n, pos}
                                            });
}


void MainWindow::sendMotorCommandControl(int x, int y, int yaw, int axis_max)
{
//    axes->setAxisData(x, y, yaw);

//    if (! m_control.en12v)
//    {
//        return;
//    }


//    // compute RPM values for drivers 1, 2, 3, 4
//    double val_x = (double)x / axis_max;
//    double val_y = (double)y / axis_max;
//    double val_yaw = (double)yaw / axis_max;
//    double omega1 = val_y;
//    double omega2 = val_y;
//    double omega3 = val_x+val_yaw;
//    double omega4 = val_x-val_yaw;

//    while (qAbs(omega3) > 1.01)
//    {
//        omega3 *= 0.99;
//    }
//    while (qAbs(omega4) > 1.01)
//    {
//        omega4 *= 0.99;
//    }

//    auto set_rpm = [&](int motor, double speed) {
//        qDebug() << "motor"<<motor<<"mapped to"<<m_options->map_to[motor];
//        switch (m_options->map_to[motor])
//        {
//        case 0:
//            if (m_control.m1en && ! m_control.m1hold)
//            {
//                m_control.m1rpm = speed * m_options->maxrpm1 * (m_options->invert1 ? -1 : 1);
//            }
//            break;
//        case 1:
//            if (m_control.m2en && ! m_control.m2hold)
//            {
//                m_control.m2rpm = speed * m_options->maxrpm2 * (m_options->invert2 ? -1 : 1);
//            }
//            break;
//        case 2:
//            if (m_control.m3en && ! m_control.m3hold)
//            {
//                m_control.m3rpm = speed * m_options->maxrpm3 * (m_options->invert3 ? -1 : 1);
//            }
//            break;
//        case 3:
//            if (m_control.m4en && ! m_control.m4hold)
//            {
//                m_control.m4rpm = speed * m_options->maxrpm4 * (m_options->invert4 ? -1 : 1);
//            }
//            break;
//        }
//    };

//    set_rpm(0, omega1);
//    set_rpm(1, omega2);
//    set_rpm(2, omega3);
//    set_rpm(3, omega4);

//    m_caterpillar->updateControl(&m_control);
}

#if AXIS_DEBUG
void MainWindow::debugSimulatedEvent()
{
    auto x = ui->dbgx->text().toInt();
    auto y = ui->dbgy->text().toInt();
    auto yaw = ui->dbgz->text().toInt();

    sendMotorCommandControl(x, y, yaw, 100);
}
#endif


void MainWindow::joyChanged()
{
#if !DISABLE_JOY
    auto status = m_joy->last();
    lprint(status->axis_1);
    lprint(status->axis_2);
    lprint(status->axis_3);
#endif
//    auto a1 = status->axis_1 - m_options->jog_zero_x;
//    auto a2 = status->axis_2 - m_options->jog_zero_y;
//    auto a3 = status->axis_3 - m_options->jog_zero_yaw;
//    qDebug() << a1 << a2 << a3;
//    auto x = qAbs(a1) > AXIS_NOISE ? a1 : 0;
//    auto y = qAbs(a2) > AXIS_NOISE ? a2 : 0;
//    auto yaw = qAbs(a3) > AXIS_NOISE ? a3 : 0;
//    if (x > AXIS_MAX) x = AXIS_MAX;
//    if (x < AXIS_MIN) x = AXIS_MIN;
//    if (y > AXIS_MAX) y = AXIS_MAX;
//    if (y < AXIS_MIN) y = AXIS_MIN;
//    if (yaw > AXIS_MAX) yaw = AXIS_MAX;
//    if (yaw < AXIS_MIN) yaw = AXIS_MIN;
//    if (m_options->invert_yaw_effect)
//        yaw = -yaw;
//    if (x == 0 && y == 0 && yaw == 0)
//    {
//        // joystick not being operated
//        //return;
//    }

//    sendMotorCommandControl(x, y, yaw, AXIS_MAX);
}


void MainWindow::zeroJog()
{
//    auto last = m_joy->last();

//    if (last == nullptr)
//    {
//        return;
//    }

//    qDebug() << "new ZERO axis coords: "
//             << last->axis_1 << ", "
//             << last->axis_2 << ", "
//             << last->axis_3;
//    m_options->jog_zero_x = last->axis_1;
//    m_options->jog_zero_y = last->axis_2;
//    m_options->jog_zero_yaw = last->axis_3;
//    m_options->persist();
}

void MainWindow::customEvent(QEvent *event)
{
    if(event->type() == EVENT_UI_INITIALIZE)
    {
        auto e = dynamic_cast<UIINitializeEvent *>(event);
        readUpdatedStatus(m_systemstatus, e->initialize_ui);
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QSettings settings;
    settings.setValue("geometry", saveGeometry());
    settings.setValue("windowState", saveState());
    QMainWindow::closeEvent(event);
}

void MainWindow::readUpdatedStatus(const SystemStatus *status, bool initialize_missing_ui_widgets)
{
//    if (thread()!=QThread::currentThread())
//    {
//        QMetaObject::invokeMethod(this, "readUpdatedStatus", Qt::QueuedConnection,
//                                  Q_ARG(const SystemStatus *, status),
//                                  Q_ARG(bool, initialize_missing_ui_widgets));
//        return;
//    }

    auto set_slider_position = [](auto slider, auto vmin, auto vmax, auto v){
        double smin = slider->minimum();
        double smax = slider->maximum();
        double sgap = smax - smin;
        if (v > vmax) v = vmax;
        if (v < vmin) v = vmin;
        double vpos = v - vmin;
        double vgap = vmax - vmin;
        double vpct = vpos / vgap;
        int spos = smin + sgap * vpct;
        slider->setValue(spos);
    };

    auto setup_led_controls = [=]()
    {
        auto parent = ui->horizontalLayout_leds;
        for (auto l: status->leds)
        {
            if (! parent->isEmpty())
            {
                parent->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
            }
            auto led_nr = l.first;
            auto led_s = std::to_string(led_nr);
            auto led_w = new QCheckBox(this);
            led_w->setTristate(true);
            auto checkState = Qt::Unchecked;
            try {
                checkState = m_options->orient_leds.at(led_nr).enabled ? Qt::PartiallyChecked : Qt::Unchecked;
            } catch (...) {}
            led_w->setCheckState(checkState);
            led_w->setObjectName(QString().sprintf("checkBox_led_status_%d", led_nr));
            led_w->setText((std::string("LED #") + led_s).c_str());
            if (checkState != Qt::PartiallyChecked)
            {
                led_w->setChecked(l.second);
            }
            parent->addWidget(led_w);
            led_controls[led_nr] = led_w;

            connect(led_w, &QCheckBox::stateChanged, [=](auto){
                if (led_w->checkState() == Qt::Checked)
                {
                    reread_status_asap = true;
                    m_streamclient->async_send_cmd<cmd_led_set>({
                        {led_s, true}
                    });
                } else if (led_w->checkState() == Qt::Unchecked)
                {
                    reread_status_asap = true;
                    m_streamclient->async_send_cmd<cmd_led_set>({
                        {led_s, false}
                    });
                }
            });
        }
    };

    auto setup_video_feed_controls = [=](){
        {
            auto available = status->video_front_available;
            auto vh = video_harnesses.at("front");
            vh->setEnabled(available);
        }
        {
            auto available = status->video_back_available;
            auto vh = video_harnesses.at("back");
            vh->setEnabled(available);
        }
        {
            auto available = status->video_orient_available;
            auto vh = video_harnesses.at("orient");
            vh->setEnabled(available);
        }
    };

    if (initialize_missing_ui_widgets)
    {
        setup_led_controls();
        setup_video_feed_controls();
    }
    std::string selected_slot = ui->comboBox_camera->currentText().toLatin1().data();
    auto i = m_systemstatus->cameras.find(selected_slot);
    if (i == m_systemstatus->cameras.end())
    {
        lerror("BUG: unmanaged slot {}", selected_slot);
    } else {
        camera_status *cs = &i->second;
        set_slider_position(ui->horizontalSlider_brightness,
                            cs->brightness_min, cs->brightness_max, cs->brightness);
        label_set_value(ui->label_visuals_brightness, cs->brightness);

        set_slider_position(ui->horizontalSlider_exposure_us,
                            cs->exposure_min, cs->exposure_max, cs->exposure);
        label_set_value(ui->label_visuals_exposure, cs->exposure);

        set_slider_position(ui->horizontalSlider_gamma,
                            cs->gamma_min, cs->gamma_max, cs->gamma);
        label_set_value(ui->label_visuals_gamma, cs->gamma);

        set_slider_position(ui->horizontalSlider_gain,
                            cs->gain_min, cs->gain_max, cs->gain);
        label_set_value(ui->label_visuals_gain, cs->gain);

        set_slider_position(ui->horizontalSlider_framerate,
                            cs->framerate_min, cs->framerate_max, cs->framerate);
        lprint(cs->framerate_min);
        lprint(cs->framerate_max);
        label_set_value(ui->label_visuals_framerate, cs->framerate);

        set_slider_position(ui->horizontalSlider_contrast,
                            cs->contrast_min, cs->contrast_max, cs->contrast);
        label_set_value(ui->label_visuals_contrast, cs->contrast);

        ui->checkBox_test_frame->setChecked( cs->test_frame );
        update_slider_labels(0);
    }

    ui->checkBox_power12v->setChecked( status->power_12v );
    ui->checkBox_battery_charger->setChecked( status->power_batterycharger );

    // update LED controls status
//    if (initialize_missing_ui_widgets)
//    {
//        for (auto l: status->leds)
//        {
//            auto i = led_controls.find(l.first);
//            if (i == led_controls.end())
//            {
//                continue;
//            }
//            auto ctrl = i->second;
//            ctrl->setChecked(l.second);
//        }
//    }

    label_set_value(ui->label_gyro_rate_x   , status->gyro_rate_x   );
    label_set_value(ui->label_gyro_rate_y   , status->gyro_rate_y   );
    label_set_value(ui->label_gyro_rate_z   , status->gyro_rate_z   );
    label_set_value(ui->label_gyro_acc_x    , status->gyro_acc_x    );
    label_set_value(ui->label_gyro_acc_y    , status->gyro_acc_y    );
    label_set_value(ui->label_gyro_acc_z    , status->gyro_acc_z    );
    label_set_value(ui->label_gyro_ang_x    , status->gyro_ang_x    );
    label_set_value(ui->label_gyro_ang_y    , status->gyro_ang_y    );
    label_set_value(ui->label_gyro_temp     , status->gyro_temp     );
//    label_set_value(ui->label_press_press   , status->press_press   );
//    label_set_value(ui->label_press_temp    , status->press_temp    );
    label_set_value(ui->label_compass_x     , status->compass_comp_x);
    label_set_value(ui->label_compass_y     , status->compass_comp_y);
    label_set_value(ui->label_compass_z     , status->compass_comp_z);
    label_set_value(ui->label_compass_angle , status->compass_angle );

    label_set_value(ui->label_power_charge  , status->power_charge);
    label_set_value(ui->label_power_voltage , status->power_tension / 1000.0);
    label_set_value(ui->label_power_current , status->power_current);

    static struct {
        QLineEdit *edit;
        unsigned int motor;
    } motor_map[] = {
        {ui->lineEdit_turn_orient, MOTOR_ID_TURN_ORIENT},
        {ui->lineEdit_focus_orient, MOTOR_ID_FOCUS_ORIENT},
        {ui->lineEdit_focus_front, MOTOR_ID_FOCUS_FRONT },
        {ui->lineEdit_focus_back, MOTOR_ID_FOCUS_BACK  },
    };


    for (auto i: motor_map)
    {
        auto p = status->motor_pos.find(i.motor);
        if (p == status->motor_pos.end())
        {
            continue;
        }

        auto val = p->second;
        QLineEdit *edit = i.edit;
        if (!edit->hasFocus())
        {
            edit->setText(QString().sprintf("%d", val));
        }
        if (i.motor == MOTOR_ID_TURN_ORIENT)
        {
            if (last_known_orient_turn_position != val)
            {
                last_known_orient_turn_position = val;
                update_auto_managed_leds_status();
            }
        }
    }

    if (initialize_missing_ui_widgets)
    {
        setEnabled(true);
    }
}

void MainWindow::update_auto_managed_leds_status()
{
    for (auto &i: m_options->orient_leds)
    {
        auto &ln = i.first;
        auto &lm = i.second;
        if (!lm.enabled)
        {
            continue;
        }

        QCheckBox *cb = findChild<QCheckBox*>(QString().sprintf("checkBox_led_status_%d", ln));
        if (cb == nullptr)
        {
            continue;
        }

        if (cb->checkState() != Qt::PartiallyChecked)
        {
            continue;
        }

        // its PartiallyChecked, so it runs in auto
        auto ol = m_options->orient_leds.find(ln);
        if (ol == m_options->orient_leds.end())
        {
            continue;
        }

        int motor_pos = last_known_orient_turn_position;
        bool status = motor_pos >= lm.min && motor_pos <= lm.max;
        m_streamclient->async_send_cmd<cmd_led_set>({
           {std::to_string(ln), status}
        });
    }
}

void MainWindow::setVideoFeedsStatus()
{
    auto video_feeds = json::object();
    auto video_subscriptions = json::array();
    for (auto &i: video_harnesses)
    {
        auto slot = i.first;
        auto vh = i.second;
        auto active = vh->enable->checkState() == Qt::Checked;
        video_feeds[slot] = active;
        if (active)
        {
            video_subscriptions.push_back(slot);
        }
    }
    reread_status_asap = true;
    m_streamclient->async_send_cmd<cmd_video>({
                                            {"video_feeds", video_feeds},
                                            {"video_subscriptions", video_subscriptions},
                                        });
}


void MainWindow::handle_new_video_frame(std::shared_ptr<video_frame_data> frame)
{
    linfo("image: slot={} w={} h={}", frame->slot, frame->width, frame->height);

    auto vhi = video_harnesses.find(frame->slot);
    if (vhi == video_harnesses.end())
    {
        lerror("ignoring image for unknown slot {}", frame->slot);
        return;
    }

    auto vh = vhi->second;

    // save image to file if needed
    if (vh->save->checkState() == Qt::Checked)
    {
        QString fname;
        fname.sprintf("%s_%s_%012lu.%04lux%04lu.rgb888",
                      m_options->session_name.toUtf8().data(),
                      frame->slot.c_str(), frame->timestamp,
                      frame->width, frame->height);
        try {
            std::ofstream outfile(fname.toUtf8().data(), std::ofstream::binary);
            outfile.write(static_cast<const std::ofstream::char_type*>(frame->data), frame->size);
        } catch (std::exception)
        {
            lerror("unable to write image to file: {}", fname.toUtf8().data());
        }
    }


    // update video widget
    vh->frame = frame; // retain ownership of new frame, free old frame buffer
    vh->image = std::make_shared<QImage>(static_cast<const uchar*>(frame->data),
                                         frame->width, frame->height, QImage::Format_RGB888);
    vh->item = std::make_shared<QGraphicsPixmapItem>(QPixmap::fromImage(*vh->image));

    auto scene = vh->view->scene();
    scene->addItem(vh->item.get());
    vh->view->setSceneRect(0, 0, frame->width, frame->height);
    vh->view->fitInView(scene->itemsBoundingRect());
    scene->update();
}


void MainWindow::send_video_params()
{
    std::string selected_slot = ui->comboBox_camera->currentText().toLatin1().data();
    auto i = m_systemstatus->cameras.find(selected_slot);
    if (i == m_systemstatus->cameras.end())
    {
        lerror("BUG: unmanaged slot {}", selected_slot);
    } else {
        linfo("sending parameters to camera slot {}", selected_slot);
        camera_status *cs = &i->second;
        auto gain = get_slider_position(ui->horizontalSlider_gain, cs->gain_min, cs->gain_max);
        auto exposure_us = get_slider_position(ui->horizontalSlider_exposure_us, cs->exposure_min, cs->exposure_max);
        auto framerate = get_slider_position(ui->horizontalSlider_framerate, cs->framerate_min, cs->framerate_max);
        auto gamma = get_slider_position(ui->horizontalSlider_gamma, cs->gamma_min, cs->gamma_max);
        auto contrast = get_slider_position(ui->horizontalSlider_contrast, cs->contrast_min, cs->contrast_max);
        auto brightness = get_slider_position(ui->horizontalSlider_brightness, cs->brightness_min, cs->brightness_max);
        bool reverse_x = ui->checkBox_reverse_x->checkState() == Qt::Checked;
        bool reverse_y = ui->checkBox_reverse_y->checkState() == Qt::Checked;
        bool test_frame = ui->checkBox_test_frame->checkState() == Qt::Checked;

        auto payload = json::object();
        payload[selected_slot] = json{
            {"auto", false},
            {"gain", gain},
            {"exposure_us", exposure_us},
            {"framerate", framerate},
            {"gamma", gamma},
            {"contrast", contrast},
            {"brightness", brightness},
            {"reverse_x", reverse_x},
            {"reverse_y", reverse_y},
            {"test_frame", test_frame},
        };
        m_streamclient->async_send_cmd<cmd_visuals_set>(payload);
        reread_status_asap = true;
    }
}

void MainWindow::update_slider_labels(int)
{
    std::string selected_slot = ui->comboBox_camera->currentText().toLatin1().data();
    auto i = m_systemstatus->cameras.find(selected_slot);
    if (i == m_systemstatus->cameras.end())
    {
        lerror("BUG: unmanaged slot {}", selected_slot);
    } else {
        camera_status *cs = &i->second;
        auto gain = get_slider_position(ui->horizontalSlider_gain, cs->gain_min, cs->gain_max);
        auto exposure_us = get_slider_position(ui->horizontalSlider_exposure_us, cs->exposure_min, cs->exposure_max);
        auto framerate = get_slider_position(ui->horizontalSlider_framerate, cs->framerate_min, cs->framerate_max);
        auto gamma = get_slider_position(ui->horizontalSlider_gamma, cs->gamma_min, cs->gamma_max);
        auto contrast = get_slider_position(ui->horizontalSlider_contrast, cs->contrast_min, cs->contrast_max);
        auto brightness = get_slider_position(ui->horizontalSlider_brightness, cs->brightness_min, cs->brightness_max);

        label_set_value(ui->label_visuals_gain, gain);
        label_set_value(ui->label_visuals_exposure, exposure_us);
        label_set_value(ui->label_visuals_framerate, framerate);
        label_set_value(ui->label_visuals_gamma, gamma);
        label_set_value(ui->label_visuals_contrast, contrast);
        label_set_value(ui->label_visuals_brightness, brightness);

    }
}
