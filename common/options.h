#ifndef OPTIONS_H
#define OPTIONS_H

#include "undef_main.h"
#include <QDialog>
#include <QUdpSocket>
#include <QTimer>
#include <map>

namespace Ui {
class OptionsDialog;
}

struct Options;

class OptionsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OptionsDialog(QWidget *parent = 0);
    ~OptionsDialog();
    int display(Options *o);

private:
    Ui::OptionsDialog *ui;
};

struct angle_bounds {
    bool enabled = false;
    int min = 0;
    int max = 0;
};

struct Options  {
    QString session_name;
    bool invert_motor_axial;
    bool invert_motor_left;
    bool invert_motor_right;
    bool invert_yaw_effect;
    QString host;
    unsigned int port;
    int jog_zero_x;
    int jog_zero_y;
    int jog_zero_yaw;
    std::map<int, angle_bounds> orient_leds;

    void load();
    void persist();
};

#endif // OPTIONS_H
