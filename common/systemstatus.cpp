#include "systemstatus.h"
#include "logging.hpp"
#include <boost/lexical_cast.hpp>
#include <set>

module_logger("status");
#define EXPOSURE_MAX 40000

camera_status::camera_status()
{
    set_exposure_max(EXPOSURE_MAX);
    set_exposure_auto_max(EXPOSURE_MAX);
}
bool camera_status::set_exposure_min     (double v) { bool res = false; if (v != exposure_min     ) res = true; exposure_min      = v; return res; }
bool camera_status::set_exposure_max     (double v) { if (v > EXPOSURE_MAX) v = EXPOSURE_MAX; bool res = false; if (v != exposure_max     ) res = true; exposure_max      = v; return res; }
bool camera_status::set_exposure         (double v) { bool res = false; if (v != exposure         ) res = true; exposure          = v; return res; }
bool camera_status::set_brightness_min   (double v) { bool res = false; if (v != brightness_min   ) res = true; brightness_min    = v; return res; }
bool camera_status::set_brightness_max   (double v) { bool res = false; if (v != brightness_max   ) res = true; brightness_max    = v; return res; }
bool camera_status::set_brightness       (double v) { bool res = false; if (v != brightness       ) res = true; brightness        = v; return res; }
bool camera_status::set_gamma_min        (double v) { bool res = false; if (v != gamma_min        ) res = true; gamma_min         = v; return res; }
bool camera_status::set_gamma_max        (double v) { bool res = false; if (v != gamma_max        ) res = true; gamma_max         = v; return res; }
bool camera_status::set_gamma            (double v) { bool res = false; if (v != gamma            ) res = true; gamma             = v; return res; }
bool camera_status::set_gain_min         (double v) { bool res = false; if (v != gain_min         ) res = true; gain_min          = v; return res; }
bool camera_status::set_gain_max         (double v) { bool res = false; if (v != gain_max         ) res = true; gain_max          = v; return res; }
bool camera_status::set_gain             (double v) { bool res = false; if (v != gain             ) res = true; gain              = v; return res; }
bool camera_status::set_framerate_min    (double v) { bool res = false; if (v != framerate_min    ) res = true; framerate_min     = v; return res; }
bool camera_status::set_framerate_max    (double v) { bool res = false; if (v != framerate_max    ) res = true; framerate_max     = v; return res; }
bool camera_status::set_framerate        (double v) { bool res = false; if (v != framerate        ) res = true; framerate         = v; return res; }
bool camera_status::set_contrast_min     (double v) { bool res = false; if (v != contrast_min     ) res = true; contrast_min      = v; return res; }
bool camera_status::set_contrast_max     (double v) { bool res = false; if (v != contrast_max     ) res = true; contrast_max      = v; return res; }
bool camera_status::set_contrast         (double v) { bool res = false; if (v != contrast         ) res = true; contrast          = v; return res; }
bool camera_status::set_exposure_auto_min(double v) { bool res = false; if (v != exposure_auto_min) res = true; exposure_auto_min = v; return res; }
bool camera_status::set_exposure_auto_max(double v) { if (v > EXPOSURE_MAX) v = EXPOSURE_MAX; bool res = false; if (v != exposure_auto_max) res = true; exposure_auto_max = v; return res; }
bool camera_status::set_test_frame       (bool   v) { bool res = false; if (v != test_frame       ) res = true; test_frame        = v; return res; }


template<typename Dest, typename Setter>
bool get_float(const nlohmann::json &c, Dest dest, Setter setter, const char *key)
{
    auto j = c.find(key);
    auto not_found = c.end();
    if (j == not_found)
    {
        lwarning("key {} not found", key);
        return false;
    }
    if (!j->is_number())
    {
        lwarning("key {} is not a number", key);
        return false;
    }
    try {
        double v = *j;
        ldebug("key {} found = {}", key, v);
        return (dest->*setter)(v);
    } catch (...)
    {
        lwarning("key {} found, but has invalid value", key);
        return false;
    }
}

template<typename Dest, typename SetterType, typename SetterClass>
bool get_value(const nlohmann::json &c, Dest dest, bool(SetterClass::*setter)(SetterType), const char *key)
{
    auto j = c.find(key);
    auto not_found = c.end();
    if (j == not_found)
    {
        lwarning("key {} not found", key);
        return false;
    }
    if (!j->is_number())
    {
        lwarning("key {} is not a number", key);
        return false;
    }
    try {
        SetterType v = *j;
        ldebug("key {} found = {}", key, v);
        return (dest->*setter)(v);
    } catch (...)
    {
        lwarning("key {} found, but has invalid value", key);
        return false;
    }
}


template<typename Dest, typename Setter>
bool get_bool(const nlohmann::json &c, Dest dest, Setter setter, const char *key)
{
    auto j = c.find(key);
    auto not_found = c.end();
    if (j == not_found)
    {
        lwarning("key {} not found", key);
        return false;
    }
    if (!j->is_boolean())
    {
        lwarning("key {} is not a boolean", key);
        return false;
    }
    try {
        bool v = *j;
        ldebug("key {} found = {}", key, v);
        (dest->*setter)(v);
        return true;
    } catch (...)
    {
        lwarning("key {} found, but has invalid value", key);
        return false;
    }
}



bool SystemStatus::set_power_batterycharger(bool v)  { bool res = v != power_batterycharger; power_batterycharger = v; return res; }
bool SystemStatus::set_power_12v(bool v)             { bool res = v != power_12v           ; power_12v = v;            return res; }
bool SystemStatus::set_power_charge (unsigned int v) { bool res = v != power_charge        ; power_charge  = v;        return res; }
bool SystemStatus::set_power_tension(unsigned int v) { bool res = v != power_tension       ; power_tension = v;        return res; }
bool SystemStatus::set_power_current(unsigned int v) { bool res = v != power_current       ; power_current = v;        return res; }
bool SystemStatus::set_version_major (int v) { bool res = v != version_major ; version_major  = v; return res; }
bool SystemStatus::set_version_minor (int v) { bool res = v != version_minor ; version_minor  = v; return res; }
bool SystemStatus::set_gyro_rate_x   (int v) { bool res = v != gyro_rate_x   ; gyro_rate_x    = v; return res; }
bool SystemStatus::set_gyro_rate_y   (int v) { bool res = v != gyro_rate_y   ; gyro_rate_y    = v; return res; }
bool SystemStatus::set_gyro_rate_z   (int v) { bool res = v != gyro_rate_z   ; gyro_rate_z    = v; return res; }
bool SystemStatus::set_gyro_acc_x    (int v) { bool res = v != gyro_acc_x    ; gyro_acc_x     = v; return res; }
bool SystemStatus::set_gyro_acc_y    (int v) { bool res = v != gyro_acc_y    ; gyro_acc_y     = v; return res; }
bool SystemStatus::set_gyro_acc_z    (int v) { bool res = v != gyro_acc_z    ; gyro_acc_z     = v; return res; }
bool SystemStatus::set_gyro_ang_x    (int v) { bool res = v != gyro_ang_x    ; gyro_ang_x     = v; return res; }
bool SystemStatus::set_gyro_ang_y    (int v) { bool res = v != gyro_ang_y    ; gyro_ang_y     = v; return res; }
bool SystemStatus::set_gyro_temp     (int v) { bool res = v != gyro_temp     ; gyro_temp      = v; return res; }
bool SystemStatus::set_press_press   (int v) { bool res = v != press_press   ; press_press    = v; return res; }
bool SystemStatus::set_press_temp    (int v) { bool res = v != press_temp    ; press_temp     = v; return res; }
bool SystemStatus::set_compass_comp_x(int v) { bool res = v != compass_comp_x; compass_comp_x = v; return res; }
bool SystemStatus::set_compass_comp_y(int v) { bool res = v != compass_comp_y; compass_comp_y = v; return res; }
bool SystemStatus::set_compass_comp_z(int v) { bool res = v != compass_comp_z; compass_comp_z = v; return res; }
bool SystemStatus::set_compass_angle (int v) { bool res = v != compass_angle ; compass_angle  = v; return res; }


void SystemStatus::readAPIStatusStructure(const nlohmann::json &data, bool do_commit)
{
    if (!data.is_object())
    {
        lwarning("expected: object");
        return;
    }


    auto parse_firmware = [=](const json &data) {
        if (!data.is_object())
        {
            lwarning("expected: object");
            return;
        }
        for (auto i=data.begin(); i!=data.end(); ++i) try
        {
            std::string m_s = i.key();
            if (m_s == "vendor")
            {
                firmware_vendor = i.value();
                continue;
            }
            if (m_s == "product")
            {
                firmware_product = i.value();
                continue;
            }
            if (m_s == "version")
            {
                firmware_version = i.value();
                continue;
            }
        } catch (std::exception &ex) {
            lerror("unable to parse firmware ids node: {}", ex.what());
        }
    }; // parse_firmware


    for (auto i = data.find("firmware"); i != data.end(); ++i)
    {
        parse_firmware(*i);
        break;
    }


    auto parse_visual_parameter_bounds = [=](const json &detected_cameras) {
        auto &data = detected_cameras;
        auto key = "detected_cameras";
        if (!data.is_object())
        {
            lwarning("expected: object");
            return;
        }
        if (data.empty())
        {
            lwarning("key {} is empty", key);
            return;
        }

        for (auto i = data.begin(); i != data.end(); ++i)
        {
            std::string slot_name = i.key();
            auto &c = i.value();
            if (!c.is_object())
            {
                lerror("camera feature data must be an object");
                continue;
            }
            auto oi = cameras.find(slot_name);
            if (oi == cameras.end())
            {
                lerror("unknown camera slot: {}", slot_name);
                continue;
            }

            camera_status &cs = oi->second;
            ldebug("detected camera for slot {}", slot_name);

            //ldebug("camera found for {} slot: {}", i.key(), c["model"]);

            int found = 0;
            found += get_float(c, &cs, &camera_status::set_exposure_min     , "exposure_min")      ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_exposure_max     , "exposure_max")      ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_brightness_min   , "brightness_min")    ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_brightness_max   , "brightness_max")    ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_gamma_min        , "gamma_min")         ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_gamma_max        , "gamma_max")         ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_gain_min         , "gain_min")          ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_gain_max         , "gain_max")          ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_framerate_min    , "framerate_min")     ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_framerate_max    , "framerate_max")     ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_contrast_min     , "contrast_min")      ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_contrast_max     , "contrast_max")      ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_exposure_auto_min, "exposure_auto_min") ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_exposure_auto_max, "exposure_auto_max") ? 1 : 0;
            if (found > 0)
            {
                dirty = true;
            }
        }
    }; // parse_visual_parameter_bounds

    auto detect_available_video_feeds = [=](const json &detected_cameras) {
        auto &data = detected_cameras;
        auto key = "detected_cameras";
        if (!data.is_object())
        {
            lwarning("expected: object");
            return;
        }
        if (data.empty())
        {
            lwarning("key {} is empty", key);
            return;
        }
        for (auto i = data.begin(); i != data.end(); ++i)
        {
            std::string slot_name = i.key();
            if (slot_name == "front")
            {
                video_front_available = true;
            }
            if (slot_name == "back")
            {
                video_back_available = true;
            }
            if (slot_name == "orient")
            {
                video_orient_available = true;
            }
        }
    }; // detect_available_video_feeds

    for (auto i = data.find("detected_cameras"); i != data.end(); ++i)
    {
        linfo("found: detected_cameras");
        parse_visual_parameter_bounds(*i);
        detect_available_video_feeds(*i);
        break;
    }

    auto parse_visual_status = [=](const json &visuals) {
        auto &data = visuals;
        auto key = "visuals";
        if (!data.is_object())
        {
            lwarning("expected: object");
            return;
        }
        if (data.empty())
        {
            lwarning("key {} is empty", key);
            return;
        }

        for (auto i = data.begin(); i != data.end(); ++i)
        {
            std::string slot_name = i.key();
            auto &c = i.value();
            if (!c.is_object())
            {
                lerror("camera feature data must be an object");
                continue;
            }
            auto oi = cameras.find(slot_name);
            if (oi == cameras.end())
            {
                lerror("unknown camera slot: {}", slot_name);
                continue;
            }

            camera_status &cs = oi->second;

            //ldebug("camera found for {} slot: {}", i.key(), c["model"]);
            int found = 0;
            found += get_float(c, &cs, &camera_status::set_exposure   , "exposure_us") ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_framerate  , "framerate")   ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_contrast   , "contrast")    ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_gain       , "gain")        ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_gamma      , "gamma")       ? 1 : 0;
            found += get_float(c, &cs, &camera_status::set_brightness , "brightness")  ? 1 : 0;
            found += get_bool (c, &cs, &camera_status::set_test_frame , "test_frame")  ? 1 : 0;
            if (found > 0)
            {
                dirty = true;
            }
        }
    }; // parse_visual_status

    for (auto i = data.find("visuals"); i != data.end(); ++i)
    {
        parse_visual_status(*i);
        break;
    }


    auto parse_motors = [=](const json &data) {
        if (!data.is_object())
        {
            lwarning("expected: object");
            return;
        }
        for (auto i=data.begin(); i!=data.end(); ++i) try
        {
            std::string m_s = i.key();
            auto m_n = boost::lexical_cast<unsigned int>(m_s);
            auto m_i = motor_pos.find(m_n);
            int val = i.value();
            linfo("motor {} at position {}", m_n, val);
            if (m_i == motor_pos.end())
            {
                motor_pos[m_n] = val;
                dirty = true;
                continue;
            }
            if (m_i->second != val)
            {
                dirty = true;
                m_i->second = val;
            }
        } catch (std::exception &ex) {
            lerror("unable to parse motors node: {}", ex.what());
        }
    }; // parse_motors


    for (auto i = data.find("motors"); i != data.end(); ++i)
    {
        parse_motors(*i);
        break;
    }


    auto parse_power = [=](const json &power) {
        auto &data = power;
        if (!data.is_object())
        {
            lwarning("expected: object");
            return;
        }
        get_bool(data, this, &SystemStatus::set_power_12v , "12v");
        get_bool(data, this, &SystemStatus::set_power_batterycharger , "battery_charger");
        get_value(data, this, &SystemStatus::set_power_charge, "charge");
        get_value(data, this, &SystemStatus::set_power_tension, "tension");
        get_value(data, this, &SystemStatus::set_power_current, "current");
    }; // parse_power


    for (auto i = data.find("power"); i != data.end(); ++i)
    {
        parse_power(*i);
        break;
    }


    auto parse_gyro = [=](const json &data) {
        if (!data.is_object())
        {
            lwarning("expected: object");
            return;
        }
        get_value(data, this, &SystemStatus::set_gyro_rate_x, "rate_x");
        get_value(data, this, &SystemStatus::set_gyro_rate_y, "rate_y");
        get_value(data, this, &SystemStatus::set_gyro_rate_z, "rate_z");
        get_value(data, this, &SystemStatus::set_gyro_acc_x , "acc_x");
        get_value(data, this, &SystemStatus::set_gyro_acc_y , "acc_y");
        get_value(data, this, &SystemStatus::set_gyro_acc_z , "acc_z");
        get_value(data, this, &SystemStatus::set_gyro_ang_x , "ang_x");
        get_value(data, this, &SystemStatus::set_gyro_ang_y , "ang_y");
        get_value(data, this, &SystemStatus::set_gyro_temp  , "temp");
    }; // parse_gyro

    for (auto i = data.find("gyro"); i != data.end(); ++i)
    {
        parse_gyro(*i);
        break;
    }


    auto parse_press = [=](const json &data) {
        if (!data.is_object())
        {
            lwarning("expected: object");
            return;
        }
        get_value(data, this, &SystemStatus::set_press_press, "press");
        get_value(data, this, &SystemStatus::set_press_temp,  "temp");
    }; // parse_press

    for (auto i = data.find("press"); i != data.end(); ++i)
    {
        parse_press(*i);
        break;
    }


    auto parse_compass = [=](const json &data) {
        if (!data.is_object())
        {
            lwarning("expected: object");
            return;
        }
        get_value(data, this, &SystemStatus::set_compass_comp_x, "comp_x");
        get_value(data, this, &SystemStatus::set_compass_comp_y, "comp_y");
        get_value(data, this, &SystemStatus::set_compass_comp_z, "comp_z");
        get_value(data, this, &SystemStatus::set_compass_angle , "angle");
    }; // parse_compass

    for (auto i = data.find("compass"); i != data.end(); ++i)
    {
        parse_compass(*i);
        break;
    }


    auto parse_leds = [=](const json &leds) {
        auto &data = leds;
        auto key = "leds";
        if (!data.is_object())
        {
            lwarning("{} key must have object value", key);
            return;
        }
        for (auto i = data.begin(); i != data.end(); ++i)
        {
            try {
                std::string led_s = i.key();
                bool led_status = i.value();
                unsigned int led_n = boost::lexical_cast<unsigned int>(led_s);
                auto led_ex = this->leds.find(led_n);
                if (led_ex == this->leds.end())
                {
                    linfo("found led mapping: {}, status {}", led_n, led_status);
                    this->leds[led_n] = led_status;
                    dirty = true;
                    continue;
                }
                if (led_ex->second != led_status)
                {
                    this->leds[led_n] = led_status;
                    dirty = true;
                }
            } catch (std::bad_cast &) {
                lerror("bad led status value or specificator (ignored)");
            }
        }
    }; // parse_leds

    for (auto i = data.find("leds"); i != data.end(); ++i)
    {
        parse_leds(*i);
        break;
    }

    auto parse_leds_mapping = [=](const json &leds) {
        auto &data = leds;
        auto key = "leds";
        if (!data.is_object())
        {
            lwarning("{} key must have object value", key);
            return;
        }
        for (auto i = data.begin(); i != data.end(); ++i)
        {
            try {
                std::string led_s = i.key();
                bool led_status = false;
                unsigned int led_n = boost::lexical_cast<unsigned int>(led_s);
                auto led_ex = this->leds.find(led_n);
                if (led_ex == this->leds.end())
                {
                    linfo("found led mapping: {}, status {}", led_n, led_status);
                    this->leds[led_n] = led_status;
                    dirty = true;
                    continue;
                }
            } catch (std::bad_cast &) {
                lerror("bad led status value or specificator (ignored)");
            }
        }
    }; // parse_leds_mapping

    for (auto i = data.find("config"); i != data.end(); ++i)
    {
        if (i.value().is_object()) {
            for (auto j = i.value().find("leds"); j != i.value().end(); ++j)
            {
                parse_leds_mapping(*j);
                break;
            }
        }
    }

    auto parse_video_subscriptions = [=](const json &data) {
        auto key = "video_subscriptions";
        if (data.empty() || data.is_null())
        {
            if (video_back_enabled)
            {
                video_back_enabled = false;
                dirty = true;
            }
            if (video_front_enabled)
            {
                video_front_enabled = false;
                dirty = true;
            }
            if (video_orient_enabled)
            {
                video_orient_enabled = false;
                dirty = true;
            }
            return;
        }
        if (!data.is_array())
        {
            lwarning("{} key must have and array value", key);
            return;
        }
        std::set<std::string> enabled_sources;

        for (auto i = data.begin(); i != data.end(); ++i)
        {
            try {
                std::string source_name = *i;
                if (cameras.find(source_name) != cameras.end())
                {
                    enabled_sources.insert(source_name);
                    continue;
                }
                lerror("unknown video source {}", source_name);
            } catch (std::bad_cast &) {
                lerror("bad video source");
            }
        }
        bool s;
        s = enabled_sources.count("back") > 0;
        if (video_back_enabled != s)
        {
            video_back_enabled = s;
            dirty = true;
        }
        s = enabled_sources.count("front") > 0;
        if (video_front_enabled != s)
        {
            video_front_enabled = s;
            dirty = true;
        }
        s = enabled_sources.count("orient") > 0;
        if (video_orient_enabled != s)
        {
            video_orient_enabled = s;
            dirty = true;
        }
    }; // parse_video_subscriptions

    for (auto i = data.find("video_subscriptions"); i != data.end(); ++i)
    {
        parse_video_subscriptions(*i);
        break;
    }

    if (do_commit)
    {
        commit();
    }
}



TreeItem::TreeItem(const QList<QVariant> &data, TreeItem *parent)
{
    m_parentItem = parent;
    m_itemData = data;
}

TreeItem::~TreeItem()
{
    qDeleteAll(m_childItems);
}

void TreeItem::appendChild(TreeItem *item)
{
    m_childItems.append(item);
}

TreeItem *TreeItem::child(int row)
{
    return m_childItems.value(row);
}

int TreeItem::childCount() const
{
    return m_childItems.count();
}

int TreeItem::row() const
{
    if (m_parentItem)
        return m_parentItem->m_childItems.indexOf(const_cast<TreeItem*>(this));

    return 0;
}

int TreeItem::columnCount() const
{
    return m_itemData.count();
}

QVariant TreeItem::data(int column) const
{
    return m_itemData.value(column);
}

TreeItem *TreeItem::parentItem()
{
    return m_parentItem;
}

TreeModel::TreeModel(const QString &data, QObject *parent)
    : QAbstractItemModel(parent)
{
    QList<QVariant> rootData;
    rootData << "Title" << "Summary";
    rootItem = new TreeItem(rootData);
    setupModelData(data.split(QString("\n")), rootItem);
}

TreeModel::~TreeModel()
{
    delete rootItem;
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent)
            const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parentItem();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());

    return item->data(index.column());
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

void TreeModel::setupModelData(const QStringList &lines, TreeItem *parent)
{
    QList<TreeItem*> parents;
    QList<int> indentations;
    parents << parent;
    indentations << 0;

    int number = 0;

    while (number < lines.count()) {
        int position = 0;
        while (position < lines[number].length()) {
            if (lines[number].at(position) != ' ')
                break;
            position++;
        }

        QString lineData = lines[number].mid(position).trimmed();

        if (!lineData.isEmpty()) {
            // Read the column data from the rest of the line.
            QStringList columnStrings = lineData.split("\t", QString::SkipEmptyParts);
            QList<QVariant> columnData;
            for (int column = 0; column < columnStrings.count(); ++column)
                columnData << columnStrings[column];

            if (position > indentations.last()) {
                // The last child of the current parent is now the new parent
                // unless the current parent has no children.

                if (parents.last()->childCount() > 0) {
                    parents << parents.last()->child(parents.last()->childCount()-1);
                    indentations << position;
                }
            } else {
                while (position < indentations.last() && parents.count() > 0) {
                    parents.pop_back();
                    indentations.pop_back();
                }
            }

            // Append a new item to the current parent's list of children.
            parents.last()->appendChild(new TreeItem(columnData, parents.last()));
        }

        ++number;
    }
}
