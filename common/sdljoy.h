#ifndef SDLJOY_H
#define SDLJOY_H

#include "undef_main.h"
#include <QDebug>
#include <QThread>
#include <QBasicTimer>
#include <QMessageBox>
#include <SDL2/SDL.h>

#define JOY_AXES_MIN 3
#define JOY_BUTTONS_MIN 2

struct JoystickStatus {
    int axis_1;
    int axis_2;
    int axis_3;
    bool btn1;
    bool btn2;
    bool btn3;
    bool btn4;
    bool btn5;
    bool btn6;
    bool btn7;
    bool btn8;

    JoystickStatus():
        axis_1(0),
        axis_2(0),
        axis_3(0),
        btn1(false),
        btn2(false),
      btn3(false),
      btn4(false),
      btn5(false),
      btn6(false),
      btn7(false),
      btn8(false)
    {}

    bool operator==(const JoystickStatus &o) const {
        return axis_1 == o.axis_1 &&
                axis_2 == o.axis_2 &&
                axis_3 == o.axis_3 &&
                btn1 == o.btn1 &&
                btn2 == o.btn2;
    }
    bool axisHasChanged(const JoystickStatus &o) const
    {
        return axis_1 != o.axis_1 ||
                axis_2 != o.axis_2 ||
                axis_3 != o.axis_3;
    }
    bool buttonHasChanged(const JoystickStatus &o) const
    {
        return btn1 != o.btn1 ||
                btn2 != o.btn2 ||
                btn3 != o.btn3 ||
                btn4 != o.btn4 ||
                btn5 != o.btn5 ||
                btn6 != o.btn6 ||
                btn7 != o.btn7 ||
                btn8 != o.btn8;
    }

};

class JoystickInput : public QObject {
    Q_OBJECT
public:

    explicit JoystickInput(QObject *parent);
    void selectDevice();
    virtual ~JoystickInput();
    const JoystickStatus *last() const;

    void readDevice();

protected:
    void timerEvent(QTimerEvent *);
signals:
    void axisHasChanged(JoystickStatus *status);
    void buttonPressed(JoystickStatus *status);

private:
    SDL_Joystick *m_device;
    bool m_gave_warning;
    JoystickStatus m_last;
    QBasicTimer *m_timer;
};

#endif // SDLJOY_H
