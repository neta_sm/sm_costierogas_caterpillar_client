#ifndef STREAM_CLIENT_H
#define STREAM_CLIENT_H 1

#include <json.hpp>
#include <logging.hpp>
#include <msgpack.hpp>
#include <future>
#include "commands.hpp"
#include "options.h"
#include <QThread>
#include <QEvent>
#include <functional>
#include <QMutex>
#include <QTcpSocket>

using json = nlohmann::json;

struct api_error: std::runtime_error
{
    using std::runtime_error::runtime_error;
};

struct api_remote_error: api_error
{
    explicit api_remote_error(cmd_error ec, const std::string &msg);
    static std::pair<cmd_error, std::string> parse_remote_error(const json &);
    cmd_error code;
};

struct api_timeout_error: api_remote_error
{
    api_timeout_error();
};


struct video_frame_data {
    msgpack::object_handle oh;
    std::string slot;
    long int orient_angle = 0;
    unsigned long int timestamp = 0;

    const void *data = nullptr;
    std::size_t size = 0;

    unsigned long int height    = 0;
    unsigned long int width     = 0;
    unsigned long int offset_x  = 0;
    unsigned long int offset_y  = 0;
    unsigned long int padding_x = 0;
    unsigned long int padding_y = 0;
    unsigned long int stride    = 0;

    std::string pixel_type     ;
};

struct pending_request;
typedef std::function<void(const pending_request &)> request_callback_t;

struct pending_request {
    pending_request(unsigned int _token, unsigned int timeout_ms);
    ~pending_request();
    QTimer *ack_timeout;

    void sent();
    void ack();
    void reply(const json &data);
    void error(cmd_error ec, const std::string &msg);
    enum class status {
        queued,
        request_sent,
        acknowledged,
        completed_ok,
        completed_error
    } current_status = status::queued;

    bool is_acknowledged() const;

    unsigned int token;
    json reply_data;
    request_callback_t on_change;
    cmd_error errcode = err_ok;
    std::string errmsg;
};



struct stream_client : QObject{
    Q_OBJECT

public:
    enum class status {
        offline,
        connecting,
        disconnected,
        connected,
        error,
    };

    stream_client(Options *o, QObject *parent=Q_NULLPTR);

    void stop();
    void start_connection();
    void cmd_ping();

    void send_cmd_ll(cmd_p req, const json &payload, request_callback_t cb);
    template<typename T>
    void send_cmd(const json &payload = json::object(), request_callback_t cb = request_callback_t())
    {
        auto req = std::make_shared<T>();
        send_cmd_ll(std::dynamic_pointer_cast<base_cmd>(req), payload, cb);
    }
    template<typename T>
    void async_send_cmd(const json &payload = json::object())
    {
        auto req = std::make_shared<T>();
        send_cmd_ll(std::dynamic_pointer_cast<base_cmd>(req), payload, [](auto&){});
    }


    bool peer_is_up_and_responsive() const;
    std::shared_ptr<pending_request> &send_request(std::shared_ptr<base_cmd> req);
    void do_read();
    void process_data(const char *data, size_t size);
    status get_status() const;
signals:
    void connected();
    void disconnected();
    void responsive();
    void unresponsive();
    void new_video_frame(std::shared_ptr<video_frame_data> vf);

private slots:
    void slot_on_connected();
    void slot_on_disconnected();
    void on_data_available();

private:
    unsigned int new_token();

    Options *options;
    QTcpSocket *socket;
    std::map<unsigned int, std::shared_ptr<pending_request>> requests;
    status current_status = status::offline;
    unsigned int next_token = 0x0;
    unsigned int ack_timeout_ms = 1000;
    unsigned int reconnection_interval = 3;
    bool last_ping_ok = false;

    enum { max_length = 1024*64 };
    char inbound_data[max_length];
    msgpack::unpacker upack;
    void check_connection();
    QTimer *status_watchdog;

    class_logger();
};



#endif // STREAM_CLIENT_H
