TEMPLATE = lib
CONFIG += staticlib

HEADERS += \
	../shared/types.hpp \
	../shared/commands.hpp \
	undef_main.h \
	logging.hpp \
	options.h \
        connection.h \
        stream_client.h \
        axisitem.h \
        mainwindow.h \
        sdljoy.h \
    systemstatus.h

SOURCES += \
	../shared/types.cpp \
        ../shared/commands.cpp \
        connection.cpp \
        options.cpp \
        stream_client.cpp \
        axisitem.cpp \
        mainwindow.cpp \
        sdljoy.cpp \
    systemstatus.cpp

FORMS += \
    mainwindow.ui \
    options.ui \
    connection.ui

include(../libs_and_paths.pro)
INCLUDEPATH += ../json/src
INCLUDEPATH += ../spdlog/include
INCLUDEPATH += ../msgpack-c/include
INCLUDEPATH += ../shared
