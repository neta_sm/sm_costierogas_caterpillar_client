#include "stream_client.h"
#include <boost/lexical_cast.hpp>
#include <QApplication>

module_logger("stream_client");

api_remote_error::api_remote_error(cmd_error ec, const std::string &msg):
    api_error(msg),
    code(ec)
{}

api_timeout_error::api_timeout_error():
    api_remote_error(err_timeout, "remote timeout")
{}

std::pair<cmd_error, std::string> api_remote_error::parse_remote_error(const json &payload)
{
    auto res = std::make_pair(err_ok, std::string(""));
    auto &code = res.first;
    auto &msg = res.second;

    if (!payload.is_object())
    {
        lerror("corrupted error structure: expected object");
        return res;
    }
    {
        auto i = payload.find("error_code");
        if (i == payload.end())
        {
            lerror("corrupted error structure: expected error_code key");
            return res;
        }
        try {
            code = i.value();
        } catch (...)
        {
            lerror("corrupted error structure: error_code key must have integer value");
        }
    }
    {
        auto i = payload.find("error_txt");
        if (i == payload.end())
        {
            lerror("corrupted error structure: expected error_txt key");
            return res;
        }
        try {
            msg = i.value();
        } catch (...)
        {
            lerror("corrupted error structure: error_txt key must have string value");
        }
    }
    return res;
}


pending_request::pending_request(unsigned int _token, unsigned int timeout_ms):
    ack_timeout(new QTimer),
    token(_token)
{
    ack_timeout->setSingleShot(true);
    ack_timeout->start(timeout_ms);
}

pending_request::~pending_request()
{
    ack_timeout->deleteLater();
}

void pending_request::sent()
{
    current_status = status::request_sent;
}
void pending_request::ack()
{
    ack_timeout->stop();
    current_status = status::acknowledged;
    if (on_change)
    {
        on_change(*this);
    }
}
void pending_request::reply(const json &data)
{
    ack_timeout->stop();
    reply_data = data;
    current_status = status::completed_ok;
    if (on_change)
    {
        on_change(*this);
    }
}
void pending_request::error(cmd_error ec, const std::string &msg)
{
    ack_timeout->stop();
    errcode = ec;
    errmsg = msg;
    current_status = status::completed_error;
    if (on_change)
    {
        on_change(*this);
    }
}

bool pending_request::is_acknowledged() const
{
    switch (current_status)
    {
    case status::acknowledged:
    case status::completed_error:
    case status::completed_ok:
        return true;
    default:
        break;
    }
    return false;
}




stream_client::stream_client(Options *o, QObject *parent):
    QObject(parent),
    options(o),
    socket(new QTcpSocket(this)),
    status_watchdog(new QTimer(this))
{
    lg.set_name(fmt::format("API peer {}:{}", options->host.toUtf8().constData(), options->port));
    connect(socket, &QTcpSocket::connected, this, &stream_client::slot_on_connected);
    connect(socket, &QTcpSocket::disconnected, this, &stream_client::slot_on_disconnected);
    connect(status_watchdog, &QTimer::timeout, this, &stream_client::check_connection);
    connect(socket, &QTcpSocket::readyRead, this, &stream_client::on_data_available);
}

void stream_client::stop()
{
    linfo("exiting service loop");
    requests.clear();
    status_watchdog->stop();
    socket->abort();
}

void stream_client::start_connection()
{
    linfo("connecting to peer {}:{}", options->host.toStdString(), options->port);
    socket->connectToHost(options->host, options->port);
}

void stream_client::slot_on_connected()
{
    linfo("connected to peer {}:{}", options->host.toStdString(), options->port);
    if (current_status != status::connected)
    {
        current_status = status::connected;
        emit connected();
    }
}

void stream_client::slot_on_disconnected()
{
    linfo("disconnected from peer {}:{}", options->host.toStdString(), options->port);
    if (current_status != status::disconnected)
    {
        current_status = status::disconnected;
        emit disconnected();
    }
}

void stream_client::check_connection()
{
    if (get_status() != status::connected && get_status() != status::offline)
    {
        socket->abort();
        start_connection();
    }
}

void stream_client::cmd_ping()
{
    auto req = std::make_shared<::cmd_ping>();
    auto pending = send_request(req);
    pending->on_change = [this, pending](auto&){
        switch (pending->current_status)
        {
        case pending_request::status::acknowledged:
            break;
        case pending_request::status::completed_ok:
            if (!last_ping_ok)
            {
                last_ping_ok = true;
                linfo("remote peer answered to ping within time limit. it is now marked as responsive");
                emit this->responsive();
            }
            break;
        case pending_request::status::completed_error:
            if (last_ping_ok)
            {
                last_ping_ok = false;
                linfo("remote peer did not answer to ping within time limit. it is now marked as unresponsive");
                emit this->unresponsive();
            }
            break;
        default:
            break;
        }
    };
}

void stream_client::send_cmd_ll(cmd_p req, const json &payload, request_callback_t cb)
{
    if (!payload.empty())
    {
        req->payload = payload;
    }
    auto pending = send_request(req);
    if (cb)
    {
        pending->on_change = cb;
    }
}

bool stream_client::peer_is_up_and_responsive() const
{
    return current_status == status::connected && last_ping_ok;
}

std::shared_ptr<pending_request> &stream_client::send_request(std::shared_ptr<base_cmd> req)
{
    if (current_status != status::connected)
    {
        raise_exception(api_error, "not connected to remote peer");
    }
    req->token = new_token();
    if (req->payload.empty())
    {
        ldebug("sending request {} with token {}, no payload", req->get_id(), req->token);
    } else {
        ldebug("sending request {} with token {}, payload = {}", req->get_id(), req->token, boost::lexical_cast<std::string>(req->payload));
    }

    auto pr = std::make_shared<pending_request>(req->token, ack_timeout_ms);
    requests[req->token] = pr;

    bytes data;
    data_from_message(data, req->produce_request());
    socket->write((const char*)&data.front(), data.size());
    ldebug("request {} with token {} enqueued, awaiting reply or ack", req->get_id(), req->token);

    connect(pr->ack_timeout, &QTimer::timeout, [this, pr, req]() {
        // timeout reached waiting for ACK from remote peer:
        // dead, unresponsive or stuck?
        ldebug("removing pending request slot with token {}: timeout", req->token);
        requests.erase(req->id);
        auto es = "timeout waiting request ack from peer";
        lerror(es);
        pr->error(err_timeout, es);
    });

    return requests[req->token];
}

void stream_client::on_data_available()
{
    auto size = socket->bytesAvailable();
    bytes data;
    data.resize(size);
    socket->read((char*)&data.front(), size);
    try {
        process_data((const char*)&data.front(), size);
    } catch(std::exception &e)
    {
        lerror("exception trapped during processing of inbound data: {}", e.what());
    }
}

void stream_client::process_data(const char *data, size_t size)
{
    upack.reserve_buffer(size);
    ::memcpy(upack.buffer(), data, size);
    upack.buffer_consumed(size);

    auto this_is_a_video_frame = [=](const msgpack::object &o){
        if (o.type == msgpack::type::ARRAY)
        {
            auto i = o.via.array.ptr;
            auto end = i + o.via.array.size;
            if (end - i >= 3)
            {
                auto &cid = i[0];
                try {
                    return cid.as<unsigned long int>() == static_cast<unsigned long int>(CMD_VIDEO_FRAME);
                } catch(...) {}
            }
        }
        return false;
    };

    auto setup_video_frame = [=](video_frame_data &frame, const msgpack::object &o){
        try {
            auto &payload = o.via.array.ptr[2];
            if (payload.type != msgpack::type::MAP)
            {
                throw std::bad_cast();
            }
            auto i = payload.via.map.ptr;
            auto end = i + payload.via.map.size;
            for (; i != end; ++i)
            {
                auto key = i->key.as<std::string>();
                if (key == "slot")
                {
                    frame.slot = i->val.as<std::string>();
                    continue;
                }
                if (key == "orient_angle")
                {
                    frame.orient_angle = i->val.as<long int>();
                    continue;
                }
                if (key == "t")
                {
                    frame.timestamp = i->val.as<unsigned long int>();
                    continue;
                }
                if (key == "h")
                {
                    frame.height = i->val.as<unsigned long int>();
                    continue;
                }
                if (key == "w")
                {
                    frame.width = i->val.as<unsigned long int>();
                    continue;
                }
                if (key == "offset_x")
                {
                    frame.offset_x = i->val.as<unsigned long int>();
                    continue;
                }
                if (key == "offset_y")
                {
                    frame.offset_y = i->val.as<unsigned long int>();
                    continue;
                }
                if (key == "padding_x")
                {
                    frame.padding_x = i->val.as<unsigned long int>();
                    continue;
                }
                if (key == "padding_y")
                {
                    frame.padding_y = i->val.as<unsigned long int>();
                    continue;
                }
                if (key == "stride")
                {
                    frame.stride = i->val.as<unsigned long int>();
                    continue;
                }
                if (key == "pixel_type")
                {
                    frame.pixel_type = i->val.as<std::string>();
                    continue;
                }
                if (key == "data")
                {
                    frame.data = static_cast<const void*>(i->val.via.bin.ptr);
                    frame.size = i->val.via.bin.size;
                    continue;
                }
            }
        } catch (std::bad_cast &)
        {
            throw std::runtime_error("invalid video frame message structure");
        }
    };


    // streaming deserialization
    auto tmp = std::make_shared<video_frame_data>();
    msgpack::object_handle &oh(tmp->oh);
    while(upack.next(oh)) {
        try {

            // check if we have a video frame
            if (this_is_a_video_frame(oh.get()))
            {
                setup_video_frame(*tmp, oh.get());
                ldebug("dispatching video {} frame for slot {}, {}x{}, {} bytes",
                       tmp->pixel_type, tmp->slot, tmp->width, tmp->height, tmp->size);
                emit new_video_frame(tmp);
                return;
            }


            auto resp = message_from_object(oh.get());
            if (resp == nullptr)
            {
                lwarning("ignored unknown message");
                return;
            }
            ldebug("received {} for {} request {}", resp->type(), resp->get_id(), resp->token);
            auto token = resp->token;
            auto i = requests.find(token);
            if (i == requests.end())
            {
                lwarning("ignoring message with unknown request token {}", token);
                return;
            }
            std::shared_ptr<pending_request> pr = i->second;
            switch (resp->type())
            {
            case cmd_reply_ack:
                pr->ack();
                break;
            case cmd_reply_completed:
                pr->reply(resp->payload);
                ldebug("removing pending request slot with token {}: answered", resp->token);
                requests.erase(i);
                break;
            case cmd_reply_error:
            {
                auto err = api_remote_error::parse_remote_error(resp->payload);
                pr->error(err.first, err.second);
                ldebug("removing pending request slot with token {}: error", resp->token);
                requests.erase(i);
            }
                break;
            default:
                lerror("received incorrect command type on client API side: {}", resp->type());
            }
        } catch (std::exception &e) {
            lerror("wire protocol error: {}", e.what());
            throw;
        }
    }
}

stream_client::status stream_client::get_status() const
{
    return current_status;
}

unsigned int stream_client::new_token()
{
    return next_token++;
}
