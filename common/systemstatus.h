#ifndef SYSTEMSTATUS_H
#define SYSTEMSTATUS_H

#include <QObject>
#include <QAbstractTableModel>
#include <json.hpp>
#include <map>

using json = nlohmann::json;

struct camera_status {
    camera_status();
    double exposure_min      = 10;
    double exposure_max      = 850000;
    double exposure          = 15000;
    double brightness_min    = -1.0;
    double brightness_max    = 1.0;
    double brightness        = 0;
    double gamma_min         = 0.25;
    double gamma_max         = 2.0;
    double gamma             = 1.0;
    double gain_min          = 0;
    double gain_max          = 24;
    double gain              = 0;
    double framerate_min     = 1;
    double framerate_max     = 5;
    double framerate         = 5;
    double contrast_min      = -1.0;
    double contrast_max      = 1.0;
    double contrast          = 0;
    double exposure_auto_min = 10;
    double exposure_auto_max = 850000;
    bool test_frame          = false;

    bool set_exposure_min     (double v);
    bool set_exposure_max     (double v);
    bool set_exposure         (double v);
    bool set_brightness_min   (double v);
    bool set_brightness_max   (double v);
    bool set_brightness       (double v);
    bool set_gamma_min        (double v);
    bool set_gamma_max        (double v);
    bool set_gamma            (double v);
    bool set_gain_min         (double v);
    bool set_gain_max         (double v);
    bool set_gain             (double v);
    bool set_framerate_min    (double v);
    bool set_framerate_max    (double v);
    bool set_framerate        (double v);
    bool set_contrast_min     (double v);
    bool set_contrast_max     (double v);
    bool set_contrast         (double v);
    bool set_exposure_auto_min(double v);
    bool set_exposure_auto_max(double v);
    bool set_test_frame       (bool   v);
};



struct SystemStatus: QObject
{
    Q_OBJECT
    using QObject::QObject;
public:

    bool dirty = true;


    bool power_batterycharger  = true;
    bool power_12v             = false;
    unsigned int power_charge  = 0;
    unsigned int power_tension = 0;
    unsigned int power_current = 0;

    bool set_power_batterycharger(bool v);
    bool set_power_12v(bool v)           ;
    bool set_power_charge (unsigned int v)     ;
    bool set_power_tension(unsigned int v)     ;
    bool set_power_current(unsigned int v)     ;

    std::map<unsigned int, int> motor_pos;
    int version_major   = 0;
    int version_minor   = 0;
    int gyro_rate_x     = 0;
    int gyro_rate_y     = 0;
    int gyro_rate_z     = 0;
    int gyro_acc_x      = 0;
    int gyro_acc_y      = 0;
    int gyro_acc_z      = 0;
    int gyro_ang_x      = 0;
    int gyro_ang_y      = 0;
    int gyro_temp       = 0;
    int press_press     = 0;
    int press_temp      = 0;
    int compass_comp_x  = 0;
    int compass_comp_y  = 0;
    int compass_comp_z  = 0;
    int compass_angle   = 0;

    bool set_version_major (int v);
    bool set_version_minor (int v);
    bool set_gyro_rate_x   (int v);
    bool set_gyro_rate_y   (int v);
    bool set_gyro_rate_z   (int v);
    bool set_gyro_acc_x    (int v);
    bool set_gyro_acc_y    (int v);
    bool set_gyro_acc_z    (int v);
    bool set_gyro_ang_x    (int v);
    bool set_gyro_ang_y    (int v);
    bool set_gyro_temp     (int v);
    bool set_press_press   (int v);
    bool set_press_temp    (int v);
    bool set_compass_comp_x(int v);
    bool set_compass_comp_y(int v);
    bool set_compass_comp_z(int v);
    bool set_compass_angle (int v);

    std::map<unsigned int, bool> leds;
    bool video_front_enabled = false;
    bool video_back_enabled = false;
    bool video_orient_enabled = false;
    bool video_front_available = false;
    bool video_back_available = false;
    bool video_orient_available = false;

    std::map<std::string, camera_status> cameras = {
        {"front", camera_status()},
        {"back", camera_status()},
        {"orient", camera_status()},
    };

    std::string firmware_product;
    std::string firmware_vendor;
    std::string firmware_version;

    void commit()
    {
        if (dirty)
        {
            dirty = false;
            emit updated(this);
        }
    }

    void readAPIStatusStructure(const nlohmann::json &data, bool commit = true);

signals:
    void updated(const SystemStatus *status);
};


class TreeItem
{
public:
    explicit TreeItem(const QList<QVariant> &data, TreeItem *parentItem = 0);
    ~TreeItem();

    void appendChild(TreeItem *child);

    TreeItem *child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    TreeItem *parentItem();

private:
    QList<TreeItem*> m_childItems;
    QList<QVariant> m_itemData;
    TreeItem *m_parentItem;
};

class TreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit TreeModel(const QString &data, QObject *parent = 0);
    ~TreeModel();

    QVariant data(const QModelIndex &index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

private:
    void setupModelData(const QStringList &lines, TreeItem *parent);

    TreeItem *rootItem;
};

#endif // SYSTEMSTATUS_H
