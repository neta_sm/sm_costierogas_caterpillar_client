#ifndef CONNECTION_H
#define CONNECTION_H

#include "undef_main.h"
#include <QDialog>

namespace Ui {
class ConnectionDialog;
}


class ConnectionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConnectionDialog(QWidget *parent = 0);
    ~ConnectionDialog();

private:
    Ui::ConnectionDialog *ui;
};


#endif // CONNECTION_H
