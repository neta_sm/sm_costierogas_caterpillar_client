#ifndef LOGGING_HPP
#define LOGGING_HPP

#include <QtGlobal>
#include <spdlog/fmt/fmt.h>
#include <spdlog/fmt/ostr.h>
#include <boost/preprocessor/facilities/overload.hpp>
#include <QDebug>



struct _class_logger_base {
    _class_logger_base()
    {
        set_name("global");
    }
    _class_logger_base(const std::string &name)
    {
        set_name(name);
    }

    template <typename FmtT, typename... Args> void trace(const FmtT &fmt, const Args&... args)
    {
        qDebug("%s", fmt::format(_name2+fmt, args...).c_str());
    }

    template <typename FmtT, typename... Args> void debug(const FmtT &fmt, const Args&... args)
    {
        qDebug("%s", fmt::format(_name2+fmt, args...).c_str());
    }

    template <typename FmtT, typename... Args> void info(const FmtT &fmt, const Args&... args)
    {
        qInfo("%s", fmt::format(_name2+fmt, args...).c_str());
    }

    template <typename FmtT, typename... Args> void warn(const FmtT &fmt, const Args&... args)
    {
        qInfo("%s", fmt::format(_name2+fmt, args...).c_str());
    }
    template <typename FmtT, typename... Args> void error(const FmtT &fmt, const Args&... args)
    {
        qCritical("%s", fmt::format(_name2+fmt, args...).c_str());
    }

    template <typename FmtT, typename... Args> void critical(const FmtT &fmt, const Args&... args)
    {
        qCritical("%s", fmt::format(_name2+fmt, args...).c_str());
    }

    void set_name(const std::string &n) const
    {
        _name = n;
        _name2 = fmt::format("[{}] ", n);
    }

    const std::string &get_name() const {
        return _name;
    }

    mutable std::string _name;
    mutable std::string _name2;
};


#define ltrace(...)   lg.trace(__VA_ARGS__)
#define ldebug(...)   lg.debug(__VA_ARGS__)
#define linfo(...)    lg.info(__VA_ARGS__)
#define lwarning(...) lg.warn(__VA_ARGS__)
#define lerror(...)   lg.error(__VA_ARGS__)
#define lfatal(...)   lg.critical(__VA_ARGS__)
#define lprint(x)     lg.debug(#x " = {}", (x))
#define ltracept      lg.trace("tracept in {} at line {}", __FILE__, __LINE__)

#define LOG_BLOCK(lvl)    if (true)

#define class_logger_0() _class_logger_base lg

#define class_logger_1(NAME) \
mutable struct _class_logger: _class_logger_base { \
    using _class_logger_base::_class_logger_base; \
    _class_logger() : _class_logger_base(NAME) { \
    } \
} lg


#define class_logger(...) BOOST_PP_OVERLOAD(class_logger_, __VA_ARGS__)(__VA_ARGS__)

#define module_logger_0() static _class_logger_base lg

#define module_logger_1(NAME) \
static struct _class_logger: _class_logger_base { \
    using _class_logger_base::_class_logger_base; \
    _class_logger() : _class_logger_base(NAME) { \
    } \
} lg

#define module_logger(...) BOOST_PP_OVERLOAD(module_logger_, __VA_ARGS__)(__VA_ARGS__)

#define raise_error(...) do { \
    auto err = fmt::format(__VA_ARGS__); \
    lerror(err); \
    throw std::runtime_error(err); \
} while(0)

#define raise_exception(cls, ...) do { \
    auto err = fmt::format(__VA_ARGS__); \
    lerror(err); \
    throw cls(err); \
} while(0)




#endif // LOGGING_HPP
