#include "options.h"
#include "ui_options.h"
#include <QSettings>



#define LEDS_MIN 0
#define LEDS_MAX 5

struct QIpAddressValidator : QValidator
{
    using QValidator::QValidator;

    State validate(QString &s, int &) const
    {
        return ip.setAddress(s) ? Acceptable : Intermediate;
    }

private:
    mutable QHostAddress ip;
};


OptionsDialog::OptionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionsDialog)
{
    ui->setupUi(this);

    {
        auto edits = {
            ui->portEdit,
        };
        for (auto i: edits)
        {
            auto validator = new QIntValidator(1, 0xffff, i);
            i->setValidator(validator);
        }
    }

    {
        auto edits = {
            ui->lineEdit_orient_led1_max,
            ui->lineEdit_orient_led1_min,
            ui->lineEdit_orient_led2_max,
            ui->lineEdit_orient_led2_min,
            ui->lineEdit_orient_led3_max,
            ui->lineEdit_orient_led3_min,
            ui->lineEdit_orient_led4_max,
            ui->lineEdit_orient_led4_min,
            ui->lineEdit_orient_led5_max,
            ui->lineEdit_orient_led5_min,
        };
        for (auto i: edits)
        {
            auto validator = new QIntValidator(-(1<<16), (1<<16), i);
            i->setValidator(validator);
        }
    }

    auto ipValidator = new QIpAddressValidator(ui->ipEdit);
    ui->ipEdit->setValidator(ipValidator);
}

OptionsDialog::~OptionsDialog()
{
    delete ui;
}

int OptionsDialog::display(Options *o)
{
    ui->edit_session_name->setText(o->session_name);
    ui->invert_axial->setChecked(o->invert_motor_axial);
    ui->invert_left->setChecked(o->invert_motor_left);
    ui->invert_right->setChecked(o->invert_motor_right);
    ui->invert_yaw_effect->setChecked(o->invert_yaw_effect);
    ui->ipEdit->setText(o->host);
    ui->portEdit->setText(QString::number(o->port));
    for (auto &i: o->orient_leds)
    {
        auto ln = i.first;
        auto &lm = i.second;
        QCheckBox *cb = this->findChild<QCheckBox*>(QString().sprintf("checkBox_orient_led%d", ln));
        if (cb == nullptr)
        {
            continue;
        }
        cb->setChecked(lm.enabled);

        QLineEdit *le;
        le = this->findChild<QLineEdit*>(QString().sprintf("lineEdit_orient_led%d_min", ln));
        if (le == nullptr)
        {
            continue;
        }
        le->setText(QString::number(lm.min));

        le = this->findChild<QLineEdit*>(QString().sprintf("lineEdit_orient_led%d_max", ln));
        if (le == nullptr)
        {
            continue;
        }
        le->setText(QString::number(lm.max));
    }
    ui->checkBox_orient_led1->setChecked(o->orient_leds[1].enabled);

    auto res = QDialog::exec();
    if (res != 0)
    {
        o->session_name = ui->edit_session_name->text();
        o->invert_motor_axial = ui->invert_axial->checkState() == Qt::Checked;
        o->invert_motor_left = ui->invert_left->checkState() == Qt::Checked;
        o->invert_motor_right = ui->invert_right->checkState() == Qt::Checked;
        o->invert_yaw_effect = ui->invert_yaw_effect->checkState() == Qt::Checked;
        o->host = ui->ipEdit->text();
        o->port = ui->portEdit->text().toInt();
        for (auto ln = LEDS_MIN; ln <= LEDS_MAX; ++ln)
        {
            QCheckBox *cb = this->findChild<QCheckBox*>(QString().sprintf("checkBox_orient_led%d", ln));
            if (cb == nullptr)
            {
                continue;
            }
            o->orient_leds[ln].enabled = cb->checkState() == Qt::Checked;

            QLineEdit *le;
            le = this->findChild<QLineEdit*>(QString().sprintf("lineEdit_orient_led%d_min", ln));
            if (le == nullptr)
            {
                continue;
            }
            o->orient_leds[ln].min = le->text().toInt();

            le = this->findChild<QLineEdit*>(QString().sprintf("lineEdit_orient_led%d_max", ln));
            if (le == nullptr)
            {
                continue;
            }
            o->orient_leds[ln].max = le->text().toInt();
        }

        if (ui->checkBox_reset_ui_status->checkState() == Qt::Checked)
        {
            QSettings settings;
            settings.remove("geometry");
            settings.remove("windowState");
        }
    }
    return res;
}


void Options::load()
{
    QSettings settings;

    session_name = settings.value("session/name", "test").toString();
    invert_motor_axial = settings.value("drivers/invert_motor_axial", false).toBool();
    invert_motor_left  = settings.value("drivers/invert_motor_left",  false).toBool();
    invert_motor_right = settings.value("drivers/invert_motor_right", false).toBool();
    invert_yaw_effect  = settings.value("ui/invert_yaw_effect", false).toBool();

    host = settings.value("network/host", "192.168.68.1").toString();
    port = settings.value("network/port", 3135).toInt();

    jog_zero_x = settings.value("axis/x_zero", 0).toInt();
    jog_zero_y = settings.value("axis/y_zero", 0).toInt();
    jog_zero_yaw = settings.value("axis/yax_zero", 0).toInt();
    orient_leds.clear();
    for (auto ln = LEDS_MIN; ln <= LEDS_MAX; ++ln)
    {
        auto kactive = QString().sprintf("leds/orient_%d_active", ln);
        auto kmin = QString().sprintf("leds/orient_%d_min", ln);
        auto kmax = QString().sprintf("leds/orient_%d_max", ln);
        if (settings.contains(kactive))
        {
            orient_leds[ln].enabled = settings.value(kactive, false).toBool();
            orient_leds[ln].min = settings.value(kmin, 0).toInt();
            orient_leds[ln].max = settings.value(kmax, 0).toInt();
        }
    }
}

void Options::persist()
{
    // save options to status file
    QSettings settings;

    settings.setValue("session/name", session_name);
    settings.setValue("drivers/invert_motor_axial", invert_motor_axial);
    settings.setValue("drivers/invert_motor_left",  invert_motor_left);
    settings.setValue("drivers/invert_motor_right", invert_motor_right);
    settings.setValue("ui/invert_yaw_effect", invert_yaw_effect);

    settings.setValue("network/host", host);
    settings.setValue("network/port", port);

    settings.setValue("axis/x_zero", jog_zero_x);
    settings.setValue("axis/y_zero", jog_zero_y);
    settings.setValue("axis/yaw_zero", jog_zero_yaw);

    for (auto &i: orient_leds)
    {
        auto ln = i.first;
        auto &lm = i.second;

        auto kactive = QString().sprintf("leds/orient_%d_active", ln);
        auto kmin = QString().sprintf("leds/orient_%d_min", ln);
        auto kmax = QString().sprintf("leds/orient_%d_max", ln);
        settings.setValue(kactive, lm.enabled);
        settings.setValue(kmin, lm.min);
        settings.setValue(kmax, lm.max);
    }
}

