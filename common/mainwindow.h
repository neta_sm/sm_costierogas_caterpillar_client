#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "axisitem.h"
#include "options.h"
#include "json.hpp"
#include "commands.hpp"
#if !DISABLE_JOY
#include "sdljoy.h"
#endif

#include "undef_main.h"
#include <map>
#include <QMainWindow>
#include <QCheckBox>
#include <QTimer>
#include <QGraphicsView>
#include <QLineEdit>
#include <QImage>


struct ControlStatus;
struct stream_client;
struct SystemStatus;
struct video_frame_data;

namespace Ui {
class MainWindow;
}

struct VideoHarness {
    std::string slot;
    QCheckBox *enable = nullptr;
    QCheckBox *save = nullptr;
    QGraphicsView *view = nullptr;
    std::shared_ptr<video_frame_data> frame;
    std::shared_ptr<QImage> image;
    std::shared_ptr<QGraphicsPixmapItem> item;

    void setEnabled(bool val)
    {
        if (enable)
            enable->setEnabled(val);
        if (save)
            save->setEnabled(val);
    }
    bool isEnabled() const {
        return enable->isEnabled();
    }
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Options *o, SystemStatus *ss, QWidget *parent = 0);
    ~MainWindow();
    bool eventFilter(QObject *object, QEvent *event);

private:
    Ui::MainWindow *ui;
    QTimer *m_controltimer;
    QTimer *m_pingtimer;
#if !DISABLE_JOY
    JoystickInput *m_joy;
#endif

    void rescaleScene();
    void sendMotorCommandControl(int x, int y, int yaw, int axis_max);
    void customEvent(QEvent *event);
    void closeEvent(QCloseEvent *event);
protected:
    void resizeEvent(QResizeEvent *event);
    AxisItem *axes;
    Options *m_options;
    SystemStatus *m_systemstatus;
    stream_client *m_streamclient;
    bool reread_status_asap = false;
    std::map<unsigned int, QCheckBox*> led_controls;
    bool ui_initialized = false;


private:
    std::map<std::string, std::shared_ptr<VideoHarness>> video_harnesses;
    void displayError();
    void sendLLMotorCommand(int motor, QLineEdit *txt);
    void setLLMotorZero(int motor);
    int last_known_orient_turn_position = 0;
    void update_auto_managed_leds_status();

public slots:

    void zeroJog();
    void joyChanged();
    void readUpdatedStatus(const SystemStatus *status, bool initialize_missing_ui_widgets);
    void setVideoFeedsStatus();
    void handle_new_video_frame(std::shared_ptr<video_frame_data> frame);
    void send_video_params();
    void update_slider_labels(int);
};

#endif // MAINWINDOW_H
